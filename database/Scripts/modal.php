<?php

	header('Access-Control-Allow-Origin: *');
	require '../../database.php';
	
	$id = $_GET['uid'];
	
	$wire = new db();
	
	$query = "select * from bed, bed_type where bed.bed_id=bed_type.id and bed.room_id=$id";
	
	$result['beds'] = $wire->get_data($query);
	
	$query = "select * from amenity, amenity_type where amenity.amenity_id=amenity_type.id and amenity.room_id=$id";
	
	$result['amenity'] = $wire->get_data($query);
	
	$query = "select * from photo where room_id=$id";
	
	$result['photos'] = $wire->get_data($query);
	
	echo json_encode($result);