var authEmail = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        //proceed
        $("#proceed").click(function(){
            authEmail.onConfirm();
        });

        //resend
        $("#resend").click(function(){
            authEmail.onResend();
        });

        load("Sending code to your email..");
        $.when(authEmail.sendCode(localStorage.getItem('uid'))).then(function(data){
            stop_load();
            if(data.message == "Success"){
                if(data.email == "success"){
                    authEmail.code = data.code;
                    toastF("Email sent");
                }else{
                    toastF("Email not sent");
                }
            }else{
                toastF("Code can not be retrieved");
            }
        }, function(xhr, status, error){
            stop_load();
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF("Error: "+errorMessage + " " + error);
        });

    },
    onBack: function(e){
        e.preventDefault();
    },
    onConfirm: function(){
        load("Activating..");
        var code = $("#code").val();
        if(code == authEmail.code){
            $.when(authEmail.activate(localStorage.getItem('uid'))).then(function(data){
                stop_load();
                if(data.activated == true){
                    toastF("Success");
                    window.location = "activated-message.html";
                }else{
                    toastF("Error While Activating");
                }
            }, function(xhr, status, error){
                stop_load();
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                toastF("Error: "+errorMessage + " " + error);
            });
        }else{
            stop_load();
            toastF("Failed: Code not match");
        }
    },
    onResend: function(){

    },
    sendCode: function(uid){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/email_code.php',
            type: 'GET',
            data: {
                uid: uid
            },
            dataType: 'json'
        })
    },
    activate: function(uid){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/activate.php',
            type: 'GET',
            data: {
                uid: uid
            },
            dataType: 'json'
        })
    }
}
authEmail.init();