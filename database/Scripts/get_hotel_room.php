<?php

	header('Access-Control-Allow-Origin: *');
	require '../../database.php';
	
	$id = $_GET['uid'];
	
	$wire = new db();
	
	$query = "SELECT * FROM room WHERE hotel_id=$id";
	
	$data= $wire->get_data($query);

	for($i=0; $i<sizeof($data['items']); $i++){
        $room_id = $data['items'][$i]->id;
        
        $query = "select * from photo where photo.room_id = $room_id";
		$data['items'][$i]->photos = $wire->get_data($query);
		
		$query = "select * from amenity, amenity_type where amenity.room_id = $room_id and amenity_type.id = amenity.amenity_id";
        $data['items'][$i]->amenities = $wire->get_data($query);
    }
	
	echo json_encode($data);