var fdHome = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        this.loadDashboard();

        this.load();
    },
    onBack: function(e){
        e.preventDefault();
    },
    loadDashboard: function(){
        $.when(fdHome.loadHotel(localStorage.getItem("uid"))).then(function(data){
            $('#name').text(data.fd.items[0].name);
            $('#email').text(data.fd.items[0].email);
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage);
        });
    },
    loadHotel: function(uid){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/loadFD.php',
            type: 'GET',
            data: {
                uid: uid
            },
            dataType: 'json'
        })
    },
    load: function(){
        $.when(fdHome.loadList(localStorage.getItem("uid"))).then(function(data){
            for(var i=0; i<data.rq.items.length; i++){
                $('#cost-li').append("<tr href='#"+data.rq.items[i].idiot+"-modal' name='av' id='"+data.rq.items[i].idiot+"-samit'> <td>"+data.rq.items[i].firstname +" "+ data.rq.items[i].lastname +"</td> <td>"+data.rq.items[i].check_in+"</td> <td>"+data.rq.items[i].check_out+"</td> </tr>");
                $('#modal-apan').append("<div id='"+data.rq.items[i].idiot+"-modal'> <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID class='close-animatedModal' --> <div class='close-"+data.rq.items[i].idiot+"-modal center' style='margin-top: 10%'> <i class='material-icons'>cancel</i> </div> <div class='modal-content' style='margin-top: 10%'> <div class='row'> <div class='col s12 m12 l12'> <div class='section'> <div class='row center'> <div class='col s6 m6 l6'> <h5 class='teal-text'>"+data.rq.items[i].check_in+"</h5> </div> <div class='col s6 m6 l6'> <h5 class='teal-text'>"+data.rq.items[i].check_out+"</h5> </div> </div> </div> <div class='divider'></div> <div class='section'> <h5 class='center'>Booking Account</h5><br/> <ul class='collection' style='border: none;'> <li class='collection-item'>"+data.rq.items[i].firstname+ " "+ data.rq.items[i].lastname +" <i class='secondary-content material-icons circle'>person</i></li> </ul> </div> <div class='divider'></div> <div class='section'> <h5 class='center'>Booking Detail</h5> <ul class='collection' style='border: none;'><li class='collection-item '>Room name: "+data.rq.items[i].name+ "<i class='material-icons secondary-content'>check</i></li><li class='collection-item'>Room size: "+data.rq.items[i].size+ "<i class='material-icons secondary-content'>check</i></li><li class='collection-item'>Max Children: "+data.rq.items[i].max_children+ "<i class='material-icons secondary-content'>check</i></li><li class='collection-item'>Max Adult: "+data.rq.items[i].max_adult+ "<i class='material-icons secondary-content'>check</i></li></ul> </div> </div> </div> </div> </div>");
                


                $('#'+data.rq.items[i].idiot+'-samit').animatedModal({
                    animatedIn: 'slideInUp',
                    animatedOut: 'slideOutDown',
                    color: '#ffffff',
                    modalTarget: data.rq.items[i].idiot+'-modal'
                });

            }
            $('#loader').hide();
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage);
        });

        $.when(fdHome.loadq(localStorage.getItem("uid"))).then(function(data){
           
            for(var i=0; i<data.rq.items.length; i++){
                $('#cost-lo').append("<tr href='#"+data.rq.items[i].idiot+"-modal' id='"+data.rq.items[i].idiot+"-samit'> <td>"+data.rq.items[i].firstname +" "+ data.rq.items[i].lastname +"</td> <td>"+data.rq.items[i].check_in+"</td> <td>"+data.rq.items[i].check_out+"</td> </tr>");
                $('#modal-apan').append("<div id='"+data.rq.items[i].idiot+"-modal'> <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID class='close-animatedModal' --> <div class='close-"+data.rq.items[i].idiot+"-modal center' style='margin-top: 10%'> <i class='material-icons'>cancel</i> </div> <div class='modal-content' style='margin-top: 10%'> <div class='row'> <div class='col s12 m12 l12'> <div class='section'> <div class='row center'> <div class='col s6 m6 l6'> <h5 class='teal-text'>"+data.rq.items[i].check_in+"</h5> </div> <div class='col s6 m6 l6'> <h5 class='teal-text'>"+data.rq.items[i].check_out+"</h5> </div> </div> </div> <div class='divider'></div> <div class='section'> <h5 class='center'>Booking Account</h5><br/> <ul class='collection' style='border: none;'> <li class='collection-item'>"+data.rq.items[i].firstname+ " "+ data.rq.items[i].lastname +" <i class='secondary-content material-icons circle'>person</i></li> </ul> </div> <div class='divider'></div> <div class='section'> <h5 class='center'>Booking Detail</h5> <ul class='collection' style='border: none;'> <li class='collection-item '>Room name: "+data.rq.items[i].name+ "<i class='material-icons secondary-content'>check</i></li><li class='collection-item'>Room size: "+data.rq.items[i].size+ "<i class='material-icons secondary-content'>check</i></li><li class='collection-item'>Max Children: "+data.rq.items[i].max_children+ "<i class='material-icons secondary-content'>check</i></li><li class='collection-item'>Max Adult: "+data.rq.items[i].max_adult+ "<i class='material-icons secondary-content'>check</i></li></ul> </div> <div class='divider'></div> <div class='section center'> <a class='waves-effect waves-light btn-small bk' onclick=\"fight('"+data.rq.items[i].idiot+"')\">Accept</a> <a class='waves-effect waves-light btn-small bk red lighten-2 spac' onclick=\"gh("+data.rq.items[i].idiot+")\">Deny</a> </div> </div> </div> </div> </div>");
            
                $('#'+data.rq.items[i].idiot+'-samit').animatedModal({
                    animatedIn: 'slideInUp',
                    animatedOut: 'slideOutDown',
                    color: '#ffffff',
                    modalTarget: data.rq.items[i].idiot+'-modal'
                });
            }
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage);
        });
    },
    loadList: function(id){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/availedList.php',
            type: 'GET',
            data: {
                id: id
            },
            dataType: 'json'
        })
    },
    loadq: function(id){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/reques.php',
            type: 'GET',
            data: {
                id: id
            },
            dataType: 'json'
        })
    }
}
fdHome.init();


function fight(id){
    $.ajax({
        url: 'http://'+server_ip+'/HARA/database/accept.php',
        type: 'GET',
        data: {
            id: id
        },
        dataType: 'json',
        error: function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF("Success!");
            window.location = "fd-hotel-home.html";
        }
    }).done(function(data){
        toastF("Success!");
        window.location = "fd-hotel-home.html";
    });
}


function gh(id){
    $.ajax({
        url: 'http://'+server_ip+'/HARA/database/deny.php',
        type: 'GET',
        data: {
            id: id
        },
        dataType: 'json',
        error: function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF("Success!");
            window.location = "fd-hotel-home.html";
        }
    }).done(function(data){
            toastF("Success!");
            window.location = "fd-hotel-home.html";
    });
}