var hotelProfile = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        /*
        $('#hotel_name').val();
        $('#owner_firstname').val();
        $('#owner_lastname').val();
        $('#owner_bdate').val();
        $('#owner_address').val();
        $('#hotel_mnumber').val();
        $('#hotel_tnumber').val();
        $('#hotel_email').val();
        $('#password').val();
        $('#cpassword').val();
        $('#fd_email').val();
        $('#fd_password').val();
        $('#domain').val();
        $('#isForward').prop('checked');*/
        load("Loading..");
        $.when(this.getProfile(localStorage.getItem("uid"))).then(function(data){
            stop_load();
            $('#hotel_name').val(data.profile.items[0].name);
            $('#owner_firstname').val(data.profile.items[0].firstname);
            $('#owner_lastname').val(data.profile.items[0].lastname);
            $('#owner_bdate').val(data.profile.items[0].bdate);
            $('#owner_address').val(data.profile.items[0].address);
            $('#hotel_mnumber').val(data.profile.items[0].mobile_no);
            $('#hotel_tnumber').val(data.profile.items[0].telephone_no);
            $('#hotel_email').val(data.profile.items[0].email);
            $('#password').val(data.profile.items[0].password);
            $('#cpassword').val(data.profile.items[0].password);
            $('#domain').val(data.profile.items[0].domain);
        }, function(xhr, status, error){
            stop_load();
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage);
        });

        $('#submit').click(function(){
            load("Updating..");
            var name = $('#hotel_name').val();
            var fname = $('#owner_firstname').val();
            var lname = $('#owner_lastname').val();
            var bdate = $('#owner_bdate').val();
            var address = $('#owner_address').val();
            var mnumber = $('#hotel_mnumber').val();
            var tnumber = $('#hotel_tnumber').val();
            var email = $('#hotel_email').val();
            var password = $('#password').val();
            //$('#cpassword').val();
            var domain = $('#domain').val();

            $.ajax({
                url: 'http://'+server_ip+'/HARA/database/update_profile.php',
                type: 'GET',
                data: {
                    uid: localStorage.getItem("uid"),
                    name: name,
                    fname: fname,
                    lname: lname,
                    bdate: bdate,
                    address: address,
                    mnumber: mnumber,
                    tnumber: tnumber,
                    email: email,
                    password: password,
                    domain: domain
                },
                dataType: 'json',
                error: function(xhr, status, error){
                    stop_load();
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    toastF('Error - ' + errorMessage);
                }
            }).done(function(data){
                stop_load();
                if(data.hotel == true && data.hotel_owner == true){
                    toastF("Success");
                    window.location = "hotel-profile.html";
                }else{
                    toastF("Failed");
                }
            });
        });

    },
    onBack: function(e){
        e.preventDefault();
    },
    getProfile: function(uid){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/getProfile.php',
            type: 'GET',
            data: {
                uid: uid
            },
            dataType: 'json'
        })
    }
}
hotelProfile.init();