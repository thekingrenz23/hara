<?php
    header('Access-Control-Allow-Origin:*');
    require_once('../../database.php');

    $wire = new db();

    //Get passed data here
    $uid = $_GET['uid'];

    $query = "select * from frontdesk, account, hotel where frontdesk.id = account.id and frontdesk.hotel_id = hotel.id and account.id = $uid";

    //Returns the data['result'] = 'filled' | 'empty', data['items'] = [array of items]
    $data['fd']=$wire->get_data($query);

    //Returns the true is success | false if failed
    //$data['rq']=$wire->manipulate_data($query, 'table_name');

    echo json_encode($data);