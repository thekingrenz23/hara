<?php
    header('Access-Control-Allow-Origin: *');
	require '../../database.php';
	
	$newfilename = round(microtime(true)) . '.' . "jpg";
	move_uploaded_file($_FILES["file"]["tmp_name"], "img/" . $newfilename);
	
	$wire = new db();
	$id = $_POST['hotel_id'];
	
	$query = "INSERT INTO cover_photo VALUES(null, $id, '$newfilename')";
	
	$result['photo'] = $wire->manipulate_data($query, 'cover_photo');
	
	echo json_encode($result);
?>