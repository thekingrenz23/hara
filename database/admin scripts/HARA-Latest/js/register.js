/*Submit button*/


$('#submit').click(function(){
	load("Loading..");

	var fname = $('#fname').val();
	var lname = $('#lname').val();
	var bdate = $('#bdate').val();
	var address = $('#address').val();
	var mnumber = $('#mnumber').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var cpassword = $('#cpassword').val();
	
	if(isFilled([fname, lname, bdate, address, mnumber, email, password, cpassword])){
		if(isEquals(password,cpassword)){
			$.ajax({
				type: 'POST',
				url: 'http://'+server_ip+'/HARA/database/register_traveler.php',
				data:{
					fname: fname,
					lname: lname,
					bdate: bdate,
					address: address,
					mnumber: mnumber,
					email: email,
					password: password
				},
				dataType: 'json'			
			}).done(function(response){					
				if(response.account == true && response.profile == true){
					stop_load();
					toastF("Success");
					window.location="index.html";
				}else{
					stop_load();
					toastF("Failed");
				}
			});
		}else{
			stop_load();
			toastF("Password not match");
		}
	}else{
		stop_load();
		toastF("Pls. Fill all Fields");
	}
});



