/*Proceed Button*/
var email_verification_code;

$('#proceed').click(function(){
	load("Verifying");
	var code = $('#code').val();
	if(code == email_verification_code){
		$.ajax({
			type: 'POST',
			url: 'http://'+server_ip+'/HARA/database/activate.php',
			data: {
				uid: localStorage.getItem('uid')
			},
			dataType: 'json'
		}).done(function(response){
			if(response.activated == true){
				stop_load();
				toastF("Success");
				window.location = "index.html";
			}else{
				toast("Error While Activating");
			}
		});
		
	}else{
		stop_load();
		toastf("Code not Match");
	}
});

$('#resend').click(function(){
	send_code(localStorage.getItem('uid'));
});

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
	
   load("Finding Email");
	if(isNull(localStorage.getItem('uid'))){
		stop_load();
		toastF("No email!");
		window.location = "index.html";	
	}else{
		send_code(localStorage.getItem('uid'));
	}
}

function send_code(uid){
	load("Sending Email");
	$.ajax({
		type: 'POST',
		url: 'http://'+server_ip+'/HARA/database/email_code.php',
		data:{
			uid: uid
		},
		dataType: 'json'
	}).done(function(response){
		if(response.message = "Success"){
			stop_load();
			if(response.email == "success"){
				email_verification_code = response.code;	
				toastF("Email Sent");
			}else{
				toastF("Email not Sent");
			}
		}else{
			//stop spinner here and toast a message that the code can not be retrieved
			stop_load();
			toastF("Failed");
		}
	});
}

