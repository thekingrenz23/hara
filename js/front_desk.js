$("#add-fd").animatedModal({
	animatedIn: "slideInUp",
	animatedOut: "slideOutDown",
	color: "#ffffff",
	modalTarget:'add-fd-modal'
});

$("#renz").animatedModal({
	animatedIn: "slideInUp",
	animatedOut: "slideOutDown",
	color: "#ffffff",
	modalTarget:'renz-modal'
});

function go(){
	/*	
	var options = { dimBackground: true };
	SpinnerPlugin.activityStart("Updating user...", options);
		setTimeout(function(){
			SpinnerPlugin.activityStop();
			SpinnerPlugin.activityStart("Success...", options);
			setTimeout(function(){
				SpinnerPlugin.activityStop();
				window.location = "front-desk.html";
			},3000);
		
		},3000);*/
	
	var email = $('#fd_update_email').val();
	var password = $('#fd_update_password').val();

	if(isFilled([email, password])){
		console.log("okay");
	}else{
		console.log("pls fill fields");
	}
}

function add(){
	var email = $('#fd_add_email').val();
	var password = $('#fd_add_password').val();
	load("Saving..");
	if(isFilled([email,password])){
		$.ajax({
			url: 'http://'+server_ip+'/HARA/database/add_fr.php',
			type: 'GET',
			data:{
				email: email,
				password: password,
				uid: localStorage.getItem('uid')
			},
			dataType: 'json'
		}).done(function(response){
			if(response.status==true){
				toastF("Success");
				window.location="front-desk.html";
			}else{
				toastF("Failed");
			}
			stop_load();
		});
	}else{
		toastF("pls fill all fields");
		stop_load();
	}
}

$(document).ready(function(){
	$.ajax({
		url: 'http://'+server_ip+'/HARA/database/load_fr.php',
		type: 'GET',
		data:{
			uid: 43
		},
		dataType: 'json'
	}).done(function(response){
		if(response.result == "filled"){
			for(var i=0; i<response.items.length; i++){
				$('#cost-li').append("<tr href='#"+response.items[i].id+"-modal' id='"+response.items[i].id+"'> <td>"+response.items[i].id+"</td> <td>"+response.items[i].email+"</td> </tr>");
				
				$('#panen').append("<div id='"+response.items[i].id+"-modal'> <div class='close-"+response.items[i].id+"-modal center' style='margin-top: 10%'> <i class='material-icons'>cancel</i> </div> <div class='modal-content' style='margin-top: 10%'> <div class='row'> <div class='col s12 m12 l12'> <div class='section'> <div class='input-field inline' style='width: 100%'> <input id='fd_add_email' type='email' class='validate' value='"+response.items[i].email+"'> <label for='fd_add_email'></label> <span class='helper-text' data-error='wrong' data-success='Correct' id='card-text'></span> </div> <div class='input-field inline' style='width: 100%'> <input id='fd_add_password' type='password' class='validate' value='"+response.items[i].password+"'> <label for='fd_add_password'></label> <span class='helper-text' data-error='wrong' data-success='Correct' id='card-text'></span> </div> </div> <div class='divider'></div> <div class='section center'> <a class='waves-effect waves-light btn-small bk' onclick='add()'>update</a> </div> </div> </div> </div> </div>");				
				
				$('#'+response.items[i].id).animatedModal({
					animatedIn: 'slideInUp',
					animatedOut: 'slideOutDown',
					color: '#ffffff',
					modalTarget:response.items[i].id+'-modal'
				});
			}
		}else{
			toastF("No Record!");
		}
	});
	
	
});












