var listView = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
        $("#loader").addClass('active');
    },
    onDeviceReady: function(){
        //load search criteria
        this.loadCrit();
        var criteria = JSON.parse(localStorage.getItem("criteria"));

        //load hotels
        this.loadHotels(criteria.location);

        //Attach bitn listeners
        this.attachListiner();
    },
    onBack: function(e){
        e.preventDefault();
    },
    loadCrit: function(){
        var criteria = JSON.parse(localStorage.getItem("criteria"));
        $('#search').val(criteria.location);
        $('#date').text(criteria.checkin + " to " + criteria.checkout);
        $('#guest').text("adult: " + criteria.adult + " children: " +criteria.children);
    },
    attachListiner: function(){
        //search filters
        $("#apply-filter").click(function(){
            var max_price = parseInt($("#max-price").val());
            var min_price = parseInt($("#min-price").val());
            var min_star = parseInt($("#min-star").val());
            var max_star = parseInt($("#max-star").val());
            var min_rating = parseInt($("#min-rating").val());
            var max_rating = parseInt($("#max-rating").val());

            if(isNaN(max_price)){
                max_price = 200000;
            }

            if(isNaN(min_price)){
                min_price = 0;
            }

            if(isNaN(max_star)){
                max_star = 5;
            }

            if(isNaN(min_star)){
                min_star = 5;
            }

            if(isNaN(min_rating)){
                min_rating = 0;
            }

            if(isNaN(max_rating)){
                max_rating = 5;
            }

            if(min_rating > max_rating){
                toastF("Invalid Rating Filter");
            }

            if(min_price > max_price){
                toastF("Invalid Price Filter");
            }

            $("div[class='card']").each(function(i){
                var card = this;
                var total_star = $(this).find(".star i").length;
                var price;
                var average_rating;

                $(this).find(".price").each(function (j) {
                    price = parseInt($(this).text());
                });

                $(this).find(".average_rate").each(function (j) {
                    average_rating = parseInt($(this).text());
                });

                if((price >= min_price && price <= max_price) && (average_rating >= min_rating && average_rating <= max_rating) && (total_star >= min_star && total_star <= max_star)){
                    $(card).show();
                }else{
                    $(card).hide();
                }
                
            });

            $(".web").each(function(i){
                var card = this;
                var total_star = $(this).find(".star i").length;
                var price;
                var average_rating;

                $(this).find(".price").each(function (j) {
                    price = parseInt($(this).text());
                });

                $(this).find(".average_rate").each(function (j) {
                    average_rating = parseInt($(this).text());
                });

                if(total_star >= min_star && total_star <= max_star){
                    $(card).show();
                }else{
                    $(card).hide();
                }
                
            });
        });

        $("#filter-btn").animatedModal({
            animatedIn: "slideInUp",
            animatedOut: "slideOutDown",
            color: "#ffffff"
        });
    },
    loadHotels: function(loca){
        $.when(listView.requestHotels(loca)).then(function(data){
            $('#loader').remove();

            if(data.hotels.items.length < 1 && data.hotels_web.items.length < 1){
                toastF("No hotel available!");
                window.location = "home.html";
            }

            for(var i=0; i<data.hotels.items.length; i++){
                if(data.hotels.items[i].description == 1){
                  var fuckyeah = "<i class='material-icons'>star</i>";
                }else if(data.hotels.items[i].description == 2){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }else if(data.hotels.items[i].description == 3){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }else if(data.hotels.items[i].description == 4){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }else if(data.hotels.items[i].description == 5){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }
  
                if(data.hotels.items[i].forward == 1){
                  var ward = "viewSite('"+data.hotels.items[i].domain+"')";
                  $('#content').append("<div class='card web'> <div class='card-image'> <img src='http://"+server_ip+"/HARA/images/img/"+data.hotels.items[i].image+"'> <span class='card-title'>"+data.hotels.items[i].name+"</span> <a onclick=\""+ward+"\" class='btn-floating halfway-fab waves-effect waves-light red'><i class='material-icons'>open_in_browser</i></a> </div> <div class='card-content'> <div class='row'> <div class='col s6 m6 l6'> <i class='material-icons'>aspect_ratio</i> <strong class='average_rate'>"+data.hotels.items[i].average+"</strong><br/> <div class='divider'></div> Hotel Class <br/> <div class='star'>  "+fuckyeah+" </div> </div> <div class='col s6 m6 l6'> Best Deals <br/> Php <span class='price'>"+data.hotels.items[i].best_deal+"</span> </div> </div> </div> </div>");
                }else{
                  var ward = "yeah('"+data.hotels.items[i].hotel_id+"')";
                  $('#content').append("<div class='card'> <div class='card-image'> <img src='http://"+server_ip+"/HARA/images/img/"+data.hotels.items[i].image+"'> <span class='card-title'>"+data.hotels.items[i].name+"</span> <a onclick=\""+ward+"\" class='btn-floating halfway-fab waves-effect waves-light red'><i class='material-icons'>open_in_browser</i></a> </div> <div class='card-content'> <div class='row'> <div class='col s6 m6 l6'> <i class='material-icons'>aspect_ratio</i> <strong class='average_rate'>"+data.hotels.items[i].average+"</strong><br/> <div class='divider'></div> Hotel Class <br/> <div class='star'>  "+fuckyeah+" </div> </div> <div class='col s6 m6 l6'> Best Deals <br/> Php <span class='price'>"+data.hotels.items[i].best_deal+"</span> </div> </div> </div> </div>");
                }
            }

            localStorage.setItem('mapdata', JSON.stringify(data));

            for(var i=0; i<data.hotels_web.items.length; i++){
                if(data.hotels_web.items[i].description == 1){
                  var fuckyeah = "<i class='material-icons'>star</i>";
                }else if(data.hotels_web.items[i].description == 2){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }else if(data.hotels_web.items[i].description == 3){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }else if(data.hotels_web.items[i].description == 4){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }else if(data.hotels_web.items[i].description == 5){
                  var fuckyeah = "<i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i><i class='material-icons'>star</i>";
                }
  
                if(data.hotels_web.items[i].forward == 1){
                  var ward = "viewSite('"+data.hotels_web.items[i].domain+"')";
                  $('#content').append("<div class='card web'> <div class='card-image'> <img src='http://"+server_ip+"/HARA/images/img/"+data.hotels_web.items[i].location+"'> <span class='card-title'>"+data.hotels_web.items[i].name+"</span> <a onclick=\""+ward+"\" class='btn-floating halfway-fab waves-effect waves-light red'><i class='material-icons'>open_in_browser</i></a> </div> <div class='card-content'> <div class='row'> <div class='col s6 m6 l6'> <i class='material-icons'>aspect_ratio</i> Advertisement <br/> <div class='divider'></div> Hotel Class <br/> <div class='star'>  "+fuckyeah+" </div> </div> <div class='col s6 m6 l6'> Best Deals <br/>  <span class='price'><b>Check Website for Best Deal</b></span> </div> </div> </div> </div>");
                }else{
                  var ward = "yeah('"+data.hotels_web.items[i].hotel_id+"')";
                  $('#content').append("<div class='card'> <div class='card-image'> <img src='http://"+server_ip+"/HARA/images/img/"+data.hotels_web.items[i].location+"'> <span class='card-title'>"+data.hotels_web.items[i].name+"</span> <a onclick=\""+ward+"\" class='btn-floating halfway-fab waves-effect waves-light red'><i class='material-icons'>open_in_browser</i></a> </div> <div class='card-content'> <div class='row'> <div class='col s6 m6 l6'> <i class='material-icons'>aspect_ratio</i> Advertisement <br/> <div class='divider'></div> Hotel Class <br/> <div class='star'>  "+fuckyeah+" </div> </div> <div class='col s6 m6 l6'> Best Deals <br/>  <span class='price'><b>Check Website for Best Deal</b></span> </div> </div> </div> </div>");
                }
            }
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage);
        });
    },
    requestHotels: function(loca){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/searchHotel.php',
            type: 'GET',
            dataType: 'json',
            data: {
                loc: loca
            }
        })
    }
}
listView.init();