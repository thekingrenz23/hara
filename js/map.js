var map;
var marker;

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 14
	});

	marker = new google.maps.Marker({
		map: map,
		position: {lat: -34.397, lng: 150.644},
		title: "Hotel X"
	});
}