<?php
    header('Access-Control-Allow-Origin:*');
    require_once('../../database.php');

    $wire = new db();

    //Get passed data here
    $id = $_GET['id'];

    $query = "DELETE FROM reservation WHERE id=$id";

    $data['rq']=$wire->manipulate_data($query, 'room');

    echo json_encode($data);