var hotelView = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        this.loadHotel();
    },
    onBack: function(e){
        e.preventDefault();
    },
    loadHotel: function(){
        $.when(hotelView.getHotelData(localStorage.getItem("hotel"))).then(function(data){
            $('#cover').attr('src',"http://"+server_ip+"/HARA/images/img/"+data.hotel.items[0].img);
            $('#hotel_name').text(data.hotel.items[0].name);
            var star="";
            for(var i=0; i<data.hotel.items[0].description; i++){
                star+="<i class='material-icons'>star</i>";
            }
            $('#hotel_class').append(star);
            $('#average_rating').text(data.hotel.items[0].average);
            $('#cleanliness').text(data.hotel.items[0].cleanliness);
            var idol = (data.hotel.items[0].cleanliness * 10) * 2;
            $('#cleanliness_bar').css('width', idol+ '%');
            $('#service').text(data.hotel.items[0].service);
            idol = (data.hotel.items[0].service * 10) * 2;
            $('#service_bar').css('width', idol+ '%');
            $('#location').text(data.hotel.items[0].location);
            var idol = (data.hotel.items[0].location * 10) * 2;
            $('#location_bar').css('width', idol+ '%');
            $('#rooms').text(data.hotel.items[0].rooms);
            var idol = (data.hotel.items[0].rooms * 10) * 2;
            $('#rooms_bar').css('width', idol+ '%');
            $('#address').text(data.hotel.items[0].address);
            $('#email').text(data.hotel.items[0].email);
            $('#telephone').text(data.hotel.items[0].telephone_no);
            $('#phone').text(data.hotel.items[0].mobile_no);

            for(var x=0; x<data.ams.items.length; x++){
                $('#am').append("<li class='center'><i class='material-icons'>"+data.ams.items[x].icon+"</i><br/>"+data.ams.items[x].name+"</li>");
            }

            $('.frame').sly({
                slidee : $('.slidee'),
                horizontal: true,
                itemNav: 'forceCentered',
                smart: false,
                activateOn: 'click',
                touchDragging: true,
                mouseDragging: true,
                scrollSource: null,
                scrollBy: 1,
                scrollHijack: 300,
                scrollTrap: true,
                elasticBounds: false
              },{
                load: function(){
      
                },
                move: [
                  function(){
      
                  },
                  function(){
      
                  }
                ]
              });
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage);
        });
    },
    getHotelData: function(hotel){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/hotelData.php',
            type: 'GET',
            data: {
                uid: hotel
            },
            dataType: 'json'
        })
    }
}
hotelView.init();