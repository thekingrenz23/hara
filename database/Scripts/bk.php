<?php

	header('Access-Control-Allow-Origin: *');
	require '../../database.php';
	
	$wire = new db();
	
	$query = "SELECT * FROM bed_type";
	
	$result = $wire->get_data($query);
	
	echo json_encode($result);