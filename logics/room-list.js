var roomList = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        this.loadRooms();
    },
    onBack: function(e){
        e.preventDefault();
    },
    loadRooms: function(){
        $.when(roomList.getRooms(localStorage.getItem('hotel'), JSON.parse(localStorage.getItem('criteria')))).then(function(data){
            
            for(var i=0; i<data.rooms.items.length; i++){
                var name = data.rooms.items[i].name; 
                var price = data.rooms.items[i].price_per_night;
                var max = parseInt(data.rooms.items[i].max_children) + parseInt(data.rooms.items[i].max_adult);
                var adult = data.rooms.items[i].max_adult;
                var child = data.rooms.items[i].max_children;
                var roomID = data.rooms.items[i].id;
                var image = "http://"+server_ip+"/HARA/images/img/"+data.rooms.items[i].photos.items[0].location;
                var included = "";
                var optional = "";
                var modal = "<div id='view-"+roomID+"-modal'> <div class='close-view-"+roomID+"-modal center' style='margin-top: 10%'> <i class='material-icons'>cancel</i> </div> <div class='modal-content' style='margin-top: 10%'> <div class='row'> ";
                var pictures = "";
                var modalEnd =  "</div> </div> </div>";

                for(var k=0; k<data.rooms.items[i].photos.items.length; k++){
                    pictures += "<div class='col s12 m7'> <div class='card'> <div class='card-image'> <img src='http://team3gmanager.000webhostapp.com/HARA/images/img/"+data.rooms.items[i].photos.items[k].location+"'> <span class='card-title'>"+name+"</span> </div> <div class='card-action'> <a class='waves-effect waves-light btn' style='width: 100%' onclick='view(\"http://team3gmanager.000webhostapp.com/HARA/images/img/"+data.rooms.items[i].photos.items[k].location+"\")'> View Photo </a> </div> </div> </div>";
                }
                modal+=pictures;
                modal+=modalEnd;
                for(var j=0; j<data.rooms.items[i].amenities.items.length; j++){
                    if(data.rooms.items[i].amenities.items[j].price == null){
                        included +="<i class='material-icons'>check</i>"+data.rooms.items[i].amenities.items[j].name+"<br>";
                    }else{
                        optional +="<i class='material-icons'>check</i>"+data.rooms.items[i].amenities.items[j].name + " : PHP "+ data.rooms.items[i].amenities.items[j].price+"<br>";
                    }
                }

                $('#room-content').append("<div class='col s12 m12 l12'> <div class='card'> <div class='card-image'> <img src='"+image+"'> <span class='card-title'>"+name+"</span> <a onclick='payment("+roomID+","+price+")' class='btn-floating halfway-fab waves-effect waves-light red'><i class='material-icons'>credit_card</i></a> </div> <div class='card-content'> <div class='row'> <div class='col s6 m6 l6'> <small>Included: </small><br/> "+included+"<small>Optional: </small><br/>"+optional+"<div class='divider'></div> </div> <div class='col s6 m6 l6'> <strong>Price Per Night:</strong> <br/>Php "+price+" <br/> <strong>Max Guest: "+max+"</strong> <br/> "+adult+" Adult <br/> "+child+" child </div> <div class='col s12 m12 l12'> <br/> <a  id='room-"+roomID+"-btn' class='waves-effect waves-light btn block' href='#view-"+roomID+"-modal'><i class='material-icons right'>expand_less</i>View more Photo</a> </div> </div> </div> </div> </div>");
                $('#photos-modal').append(modal);

                $('#room-'+roomID+'-btn').animatedModal({
                    animatedIn: 'slideInUp',
                    animatedOut: 'slideOutDown',
                    color: '#ffffff',
                    modalTarget:'view-'+roomID+'-modal'
                });
                
                $('#loader').hide();
            }
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage + " " + error);
        });
    },
    getRooms: function(uid , crit){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/getRoom.php',
            type: 'GET',
            data: {
                uid: uid,
                checkin: crit.checkin,
                checkout: crit.checkout,
                child: crit.children,
                adult: crit.adult
            },
            dataType: 'json'
        })
    },
    getPhotos: function(roomID){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/getRoomPhotos.php',
            type: 'GET',
            data: {
                id: roomID
            },
            dataType: 'json'
        })
    },
    getAmenities: function(roomID){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/getAmenities.php',
            type: 'GET',
            data: {
                id: roomID
            },
            dataType: 'json'
        })
    }
}
roomList.init();