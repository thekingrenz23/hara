<?php

	//Import Requirements
	header("Access-Control-Allow-Origin: *");
	require '../../database.php';
	
	//get data
	$table = $_GET['table'];
	
	//create new db connection
	$wire = new db();
	
	$ans = $wire->check_changes($table);
	
	echo json_encode($ans);