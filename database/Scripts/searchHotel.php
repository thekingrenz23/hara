<?php
    header('Access-Control-Allow-Origin:*');
    require_once('../../database.php');

    $wire = new db();

    //Get passed data here
    $loc = $_GET['loc'];
    
    $query = "SELECT hotel.name, hotel.description, hotel.domain,location.*, ROUND((vr.cleanliness + vr.location + vr.rooms + vr.service)/4,1) AS average, room.price_per_night AS best_deal, cover_photo.location AS image , hotel.forward , hotel.id FROM hotel, location, average_rating AS vr, room, cover_photo WHERE hotel.id= location.hotel_id AND cover_photo.hotel_id = hotel.id AND vr.hotel_id = hotel.id AND room.hotel_id = hotel.id AND room.units > 0 AND room.price_per_night = (SELECT MIN(price_per_night) FROM room WHERE room.hotel_id = hotel.id) AND location.address LIKE '%$loc%'";

    //Returns the data['result'] = 'filled' | 'empty', data['items'] = [array of items]
    $data['hotels']=$wire->get_data($query);

    $query = "SELECT * FROM hotel ,location, cover_photo where hotel.id = location.hotel_id AND cover_photo.hotel_id = hotel.id AND hotel.id = location.hotel_id and hotel.forward= 1 and location.address like '%$loc%'";

    $data['hotels_web']=$wire->get_data($query);
    //Returns the true is success | false if failed
    //$data['rq']=$wire->manipulate_data($query, 'table_name');

    echo json_encode($data);