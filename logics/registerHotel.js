var registerHotel = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        //StatusBar.backgroundColorByHexString("#ff0000");
        registerHotel.lat = null;
        registerHotel.lng = null;
        $('#submit').click(function(){
            navigator.notification.confirm(
                'HARA',
                 function(index){
                    if(index == 1){
                        navigator.camera.getPicture(function(img){
                            registerHotel.registerNow(img);
                        }, function(error){
                            toastF(error);
                        }, {destinationType: Camera.DestinationType.FILE_URI, sourceType: Camera.PictureSourceType.PHOTOLIBRARY});
                    }
                 },            
                'One last step! Hara requires an image of your hotel that will serve as a cover photo. Please click next to continue!',           // title
                ['Next','cancel']     
            );
        });
    },
    onBack: function(e){
        e.preventDefault();
    },
    registerNow: function(img){
        var hotel_name = $('#hotel_name').val();
        var owner_firstname = $('#owner_firstname').val();
        var owner_lastname = $('#owner_lastname').val();
        var owner_bdate = $('#owner_bdate').val();
        var owner_address = $('#owner_address').val();
        var hotel_mnumber = $('#hotel_mnumber').val();
        var hotel_tnumber = $('#hotel_tnumber').val();
        var hotel_email = $('#hotel_email').val();
        var password = $('#password').val();
        var cpassword = $('#cpassword').val();
        var domain = $('#domain').val();
        var fd_email = $('#fd_email').val();
        var fd_password = $('#fd_password').val();
        var isForward = $('#isForward').prop('checked');

        var forward;
        if(isForward){
            forward = 1;
        }else{
            forward = 0;
        }
        
        if(!isFilled([domain])){
            domain = "no domain";
        }
        load("Registering..");
        if(isFilled([hotel_name, owner_firstname, owner_lastname, owner_bdate, owner_address, hotel_mnumber, hotel_tnumber, hotel_email, password, cpassword, fd_email, fd_password])){
            if(password == cpassword){
                if(isNull(registerHotel.lat) && isNull(registerHotel.lng)){
                    //get coordinates and save
                    $.when(registerHotel.getCoords(owner_address)).then(function(data){
                        registerHotel.lat = data.results[0].geometry.location.lat;
                        registerHotel.lng = data.results[0].geometry.location.lng;
                  
                        if(data.status = "OK"){
                            //before saving check if email is existing
                            $.when(registerHotel.checkEmail(hotel_email)).then(function(data){
                                if(data.exist == true){
                                    stop_load();
                                    toastF("Email Already Exist");
                                }else{
                                    //save to database
                                    $.when(registerHotel.saveHotel()).then(function(data){
                                       
                                        if(data.account == true && data.profile == true){
                                            
                                            localStorage.setItem("uid", data.id);
                                            
                                            window.resolveLocalFileSystemURI(img, function(fileEntry){
                                                var fd = new FormData();
                                                fileEntry.file(function(file){
                                                    var reader = new FileReader();
                                                    reader.onloadend = function(e) {
                                                        var imgBlob = new Blob([ this.result ], { type: "image/jpeg" } );
                                                        fd.append('file', imgBlob);
                                                        fd.append('hotel_id', data.id);
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: 'http://'+server_ip+'/HARA/images/upload_cover.php',
                                                            cache:false,
                                                            contentType: false,
                                                            processData: false,
                                                            data: fd,
                                                            dataType: 'json',
                                                            error: function(){
                                                                toastF("Connection Error");
                                                            }
                                                        }).done(function(response){
                                                            stop_load();
                                                            if(response.photo == true){
                                                                toastF("Succes");
                                                                window.location = "login-auth-email.html";
                                                            }else{
                                                                toastF("Error");
                                                            }
                                                        });
                                                    };
                                                    reader.readAsArrayBuffer(file);
                                                });
                                            }, function(){
                                                toastF("Fail to upload photo");
                                            });
                                        }else{
                                            toastF("Failed!");
                                        }
                                    }, function(xhr, status, error){
                                        stop_load();
                                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                                        toastF("last step Error: "+errorMessage + " " + error);
                                    });
                                }
                            }, function(xhr, status, error){
                                stop_load();
                                var errorMessage = xhr.status + ': ' + xhr.statusText;
                                toastF("email Error: "+errorMessage + " " + error);
                            });
                        }else{
                            stop_load();
                            toastF("Error getting coordinates!");
                        }
                    }, function(xhr, status, error){
                        stop_load();
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        toastF("Error: "+errorMessage + " " + error);
                    });
                }else{
                    //get coordinates and save
                    $.when(registerHotel.getCoords(owner_address)).then(function(data){
                        registerHotel.lat = data.results[0].geometry.location.lat;
                        registerHotel.lng = data.results[0].geometry.location.lng;
                        if(data.status = "OK"){
                            //before saving check if email is existing
                            $.when(registerHotel.checkEmail(hotel_email)).then(function(data){
                                if(data.exist == true){
                                    stop_load();
                                    toastF("Email Already Exist");
                                }else{
                                    //save to database
                                    $.when(registerHotel.saveHotel()).then(function(data){
                                        
                                        if(data.account == true && data.profile == true){
                                            
                                            localStorage.setItem("uid", data.id);
                                     
                                            window.resolveLocalFileSystemURI(img, function(fileEntry){
                                                var fd = new FormData();
                                                fileEntry.file(function(file){
                                                    var reader = new FileReader();
                                                    reader.onloadend = function(e) {
                                                        var imgBlob = new Blob([ this.result ], { type: "image/jpeg" } );
                                                        fd.append('file', imgBlob);
                                                        fd.append('hotel_id', data.id);
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: 'http://'+server_ip+'/HARA/images/upload_cover.php',
                                                            cache:false,
                                                            contentType: false,
                                                            processData: false,
                                                            data: fd,
                                                            dataType: 'json',
                                                            error: function(){
                                                                toastF("Connection Error");
                                                            }
                                                        }).done(function(response){
                                                            stop_load();
                                                            if(response.photo == true){
                                                                toastF("Succes");
                                                                window.location = "login-auth-email.html";
                                                            }else{
                                                                toastF("Error");
                                                            }
                                                        });
                                                    };
                                                    reader.readAsArrayBuffer(file);
                                                });
                                            }, function(){
                                                toastF("Fail to upload photo");
                                            });
                                        }else{
                                            toastF("Failed!");
                                        }
                                    }, function(xhr, status, error){
                                        stop_load();
                                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                                        toastF("last step Error: "+errorMessage + " " + error);
                                    });
                                }
                            }, function(xhr, status, error){
                                stop_load();
                                var errorMessage = xhr.status + ': ' + xhr.statusText;
                                toastF("email Error: "+errorMessage + " " + error);
                            });
                        }else{
                            stop_load();
                            toastF("Error getting coordinates!");
                        }
                    }, function(xhr, status, error){
                        stop_load();
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        toastF("Error: "+errorMessage + " " + error);
                    });
                }	
            }else{
                stop_load();
                toastF("Password not matched!");
            }
        }else{
            stop_load();
            toastF("Pls. fill required fields");
        }

    },
    getCoords: function(address){
        return $.ajax({
            url: 'https://maps.googleapis.com/maps/api/geocode/json',
            type: 'GET',
            data: {
                address: address.replace(/\s/g, ''),
				key: 'AIzaSyCeaDB-C0Z0sxoOjSlip57IhVNfJgOie1o'
            },
            dataType: 'json'
        })
    },
    checkEmail: function(email){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/check_dup_email.php',
            type: 'GET',
            data: {
                email: email
            },
            dataType: 'json'
        })
    },
    saveHotel: function(){
        var hotel_name = $('#hotel_name').val();
        var owner_firstname = $('#owner_firstname').val();
        var owner_lastname = $('#owner_lastname').val();
        var owner_bdate = $('#owner_bdate').val();
        var owner_address = $('#owner_address').val();
        var hotel_mnumber = $('#hotel_mnumber').val();
        var hotel_tnumber = $('#hotel_tnumber').val();
        var hotel_email = $('#hotel_email').val();
        var password = $('#password').val();
        var cpassword = $('#cpassword').val();
        var fd_email = $('#fd_email').val();
        var fd_password = $('#fd_password').val();
        var domain = $('#domain').val();
        var isForward = $('#isForward').prop('checked');

        var forward;
        if(isForward){
            forward = 1;
        }else{
            forward = 0;
        }

        if(!isFilled([domain])){
            domain = "no domain";
        }

        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/register_hotelier.php',
            type: 'GET',
            data: {
                hotel_name : hotel_name,
                owner_firstname : owner_firstname,
                owner_lastname : owner_lastname,
                owner_bdate : owner_bdate,
                owner_address: owner_address,
                hotel_mnumber : hotel_mnumber,
                hotel_tnumber: hotel_tnumber,
                hotel_email: hotel_email,
                password: password,
                domain: domain,
                isForward : forward,
                lat: registerHotel.lat,
                lng: registerHotel.lng,
                fd_email: fd_email,
                fd_password: fd_password
            },
            dataType: 'json'
        })
    }
}
registerHotel.init();