var index = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        //login button
        $('#log-in').click(function(){
            index.onLogin();
        });

        //signup button
        $('#sign-up').click(function(){
            index.onSignup();
        });

        //forgot password button
        $('#forgot-password').click(function(){
            index.onForgotPassword();
        });
    },
    onBack: function(e){
        e.preventDefault();
    },
    onLogin: function(){
        load("Logging in..");
        
        var pass = $('#pass').val();
        var email = $('#email').val();
       
        if(isFilled([pass, email])){
            $.when(index.getAccount(pass, email)).then(function(data){
                
                if(data.legit == true){
                    if(data.type == "hotelier"){
                        if(data.activated == true){
                            localStorage.setItem('uid',data.id);
                            $.when(index.hotelierType(email)).then(function(data){
                                if(data.message == "success"){
                                    stop_load();
                                    if(data.forward == 1){
                                        window.location = "web-hotel-home.html";
                                    }else{
									    window.location = "noweb-hotel-home.html";
                                    }
                                }else{
                                    stop_load();
                                    toastF("Email not Found");
                                }
                            }, function(xhr, status, error){
                                stop_load();
                                var errorMessage = xhr.status + ': ' + xhr.statusText;
                                toastF("Hotelier type Error: "+errorMessage + " " + error);
                            });
                        }else if(data.activated == false){
                            localStorage.setItem('uid',data.id);
                            $.when(index.getForward(data.id)).then(function(data){
                                stop_load();
                                if(data.forward == 2){
                                    navigator.notification.confirm(
                                        'HARA requires hotels to verify there mobile number! Pls. continue to verify number!', 
                                         function(index){
                                            if(index == 1){
                                                window.location = "https://developer.globelabs.com.ph/dialog/oauth/7b7pFMe5zLh5rTGrazi5g6hq6b68FE4n/";
                                            }
                                         },            
                                        'HARA',           
                                        ['continue','cancel']     
                                    );
                                }else if(data.forward == 3){
                                    navigator.notification.alert(
                                        'HARA is still verifying your account!',
                                        function(){},
                                        'HARA',
                                        'okay'
                                    );
                                }else{
                                    window.location = "login-auth-email.html";
                                }
                            }, function(xhr, status, error){
                                stop_load();
                                var errorMessage = xhr.status + ': ' + xhr.statusText;
                                toastF("Error: "+errorMessage + " " + error);
                            });
                        }
                    }else if(data.type == "traveler"){
                        localStorage.setItem('uid', data.id);
                        stop_load();
                        window.location = "home.html";
                    }else if(data.type == "frontdesk"){
                        localStorage.setItem('uid', data.id);
                        stop_load();
                        window.location= "fd-hotel-home.html";
                    }
                }else{
                    stop_load();
                    toastF(data.message);
                }

            },function(xhr, status, error){
                stop_load();
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                toastF("Error: "+errorMessage + " " + error);
            });  
        }else{
            stop_load();
            toastF("Email and Password is required");
        }
    },
    onSignup: function(){
        window.location = "register.html";
    },
    onForgotPassword: function(){
        window.location = "forgot-password.html";
    },
    getAccount: function(pass, email){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/auth_user.php',
            type: 'GET',
            data: {
                email: email,
                password: pass
            },
            dataType: 'json'
        })
    },
    getForward: function(uid){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/accepted.php',
            type: 'GET',
            data: {
                uid: uid
            },
            dataType: 'json'
        })
    },
    hotelierType: function(email){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/forward_list.php',
            type: 'GET',
            data: {
                email: email
            },
            dataType: 'json'
        });
    }
}
index.init();
