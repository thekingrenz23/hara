/*
SQLyog Community v13.1.1 (32 bit)
MySQL - 10.1.36-MariaDB : Database - hara
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hara` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hara`;

/*Table structure for table `amenities` */

DROP TABLE IF EXISTS `amenities`;

CREATE TABLE `amenities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `amenity_id` int(11) DEFAULT NULL,
  `free` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `amenity_to_type` (`amenity_id`),
  KEY `amenities_to_rooms` (`room_id`),
  CONSTRAINT `amenities_to_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `amenity_to_type` FOREIGN KEY (`amenity_id`) REFERENCES `amenities_type` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `amenities` */

/*Table structure for table `amenities_type` */

DROP TABLE IF EXISTS `amenities_type`;

CREATE TABLE `amenities_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `price` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `amenities_type` */

insert  into `amenities_type`(`id`,`name`,`description`,`price`) values 
(1,'Parking','When you stay with us, your parking is complimentary, and you can come and go as you please. No valet or parking garage.',0.00),
(2,'Fridge','Dont worry about your frozen foods we can handle that',NULL),
(3,'Flat Tv','Entertain yourselve with our Flat Tv watch your favorite movie or channels',NULL),
(4,'Wifi','Connect with your friends and family',NULL),
(5,'Hairdryer','Dry your hair in an instant',NULL),
(6,'Iron','Put up your best dress with the help of iron',NULL),
(7,'Kitchen Facility','Cook your own food',NULL),
(8,'Towel','Dry your hair and body with our towel',NULL),
(9,'Swimming Pool','Relax with our very own swimming pool',NULL);

/*Table structure for table `beds` */

DROP TABLE IF EXISTS `beds`;

CREATE TABLE `beds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `beds_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bed_to_room` (`room_id`),
  KEY `beds_to_type` (`beds_id`),
  CONSTRAINT `bed_to_room` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `beds_to_type` FOREIGN KEY (`beds_id`) REFERENCES `beds_type` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `beds` */

/*Table structure for table `beds_type` */

DROP TABLE IF EXISTS `beds_type`;

CREATE TABLE `beds_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `beds_type` */

/*Table structure for table `hotelier_profile` */

DROP TABLE IF EXISTS `hotelier_profile`;

CREATE TABLE `hotelier_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `hotel_name` varchar(200) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `telephone` varchar(30) DEFAULT NULL,
  `domain` varchar(100) DEFAULT NULL,
  `forward` tinyint(1) DEFAULT NULL,
  `description` text,
  `cancel_poplicy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hotelier_to_account` (`uid`),
  CONSTRAINT `hotelier_to_account` FOREIGN KEY (`uid`) REFERENCES `users_account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `hotelier_profile` */

insert  into `hotelier_profile`(`id`,`uid`,`hotel_name`,`firstname`,`lastname`,`birthdate`,`address`,`mobile`,`telephone`,`domain`,`forward`,`description`,`cancel_poplicy`) values 
(1,2,'Thunderbird Resorts & Casinos – Poro Point','Cesar','Dacanay','1989-12-20','Thunderbird Resorts and Casinos Poro Point, Poro Point, Freeport Zone, San Fernando, La Union','+63728887777','566-566-456-5','https://www.thunderbird-asia.com/poro-point/',1,'Nestled on a scenic 100-ft cliff at the highest point of Poro Point Peninsula that overlooks San Fernando Bay and the West Philippine Sea where one can witness the most picturesque sunsets, Thunderbird Resorts & Casinos – Port Point is a 65-hectare Mediterranean-inspired premier integrated resort in La Union, Philippines.','own'),
(2,3,'Costa Villa Beach Resort','Remy','Cariba','1989-12-20','Costa Villa Beach Resort, MacArthur Highway, San Juan, La Union','+63728887777','566-566-456-5','http://www.costavillaresort.com/',0,'Whether visiting San Juan, La Union on a holiday or attending a business conference. Simply relax in the comfort and Style of our Hotel & Apartelle at Costa Villa Beach Resort in La Union.','app'),
(3,4,'La Roca Villa Resort Hotel','Hipolito','Daren','1989-12-20','La Roca Villa Resort Hotel, San Juan, La Union','+63728887777','566-566-456-5','http://larocavilla.com/index.php?check=english',0,'La Roca Villa is a very private resort in a beautiful garden setting around the Rock\r\nThe resort is right on the surfing beach of San Juan on the north-west side of Luzon Island, the main island of the Philippine Archipelago. The distance to the capital Manila is 280 km. The ideal place to wind down and to relax. Or you may want to learn how to surf. The sea is clean and clear and the water temperature does not fall below 25°C even during «winter».\r\nWhile savoring an afternoon drink at the bar, on the Rock or at the beach terrace, you may watch the sun go down over the ocean horizon.','app'),
(4,5,'Marand Resort and Spa','Genevieve','Draken','1989-12-20','Marand Resort and Spa, MacArthur Highway, Bauang, La Union','+63728887777','566-566-456-5','http://www.marandresort.ph/main.php',1,'Escape to beautiful Marand resort located only a 10 minutes drive from Bauang Town. Marand resort\'s wide range of resort facilities cater for everyone and offer guests of all ages the perfect destination to relax and unwind. Marand\'s facilities are built around a relaxed atmosphere that lets you enjoy your favourite cocktail while having a massage or spa bath in our scenic surroundings. Your children can enjoy a range of different facilities including the duel water slide and pool and outdoor playground. We are open 9am to 9pm daily','own'),
(5,6,'Maritoni Bali Suites & Villa','Jonathan','Faken','1989-12-20','Maritoni Bali Suites and Villas, MacArthur Highway, Bauang, La Union','+63728887777','566-566-456-5','https://tengm01.wixsite.com/maritonibalisuites',0,'Maritoni Bali Suites and Villas is an affordable premium resort that is perfect for rest and relaxation. Strategically situated at the border of San Fernando and Bauang, it is far enough from the traffic and noise, but is only a short drive away from the city. La Union\'s famous surfing spots and even Baguio are easily accessible from the resort! ','app'),
(6,7,'Aureo La Union','Wame','Lee','1989-12-20','aureo resort la union','+63728887777','566-566-456-5','https://secure.staah.com/common-cgi/package/packagebooking.pl?propertyId=8001&unk=1189&header=&flexi',1,'Aureo Resort is your sanctuary to relax and unwind. From serene beaches to infinity pools, we provide a tranquil environment to recuperate after a tiresome work week. Create memories with fun-filled activities. Enjoy the fresh breeze and glide with the waves for selfie-captured moments.\r\nWe’re located in the heart of La union that is a 4-hour drive outside the metro.','own'),
(7,8,'Woods Way Inn','Franken','Lean','1989-12-20','Woods Way Inn, Dacanay Street, San Fernando, La Union','+63728887777','566-566-456-5','http://www.sanfernandocity.gov.ph/sfcsite/index.php/en/online-services/edirectory/232-hotels/155-woo',0,'Enjoy this hotel thanks','app'),
(8,9,'Cozy Cabin','Frauleen','Seth','1989-12-20','not available','+63728887777','566-566-456-5','not available',0,'not available','app'),
(9,10,'Golden Sunrise Hotel ','Brend','Sone','1989-12-20','Golden Sunrise Hotel, Rosario La Union, Rosario, La Union','+63728887777','566-566-456-5',NULL,0,'Savor your favorite dishes with special cuisines from Golden Sunrise Hotel - Rosario exclusively for you.\r\n\r\nWiFi is available within public areas of the property to help you to stay connected with family and friends.\r\n\r\nGolden Sunrise Hotel - Rosario is a hotel with great comfort and excellent service according to most hotel\'s guests.\r\n\r\nGolden Sunrise Hotel - Rosario is the ideal choice for you who are looking for a comfortable yet affordable accommodation.','app'),
(10,11,'Villas Buenavista Hotel and Restaurant','Arjay','Snoop','1989-12-20','Villas Buenavista (Hotel and Restaurant), San Juan, La Union','+63728887777','566-566-456-5','http://www.villasbuenavista.ph/',0,'Have an enjoyable and relaxing day at the pool, whether you’re traveling solo or with your loved ones.\r\n\r\nSavor your favorite dishes with special cuisines from Villas Buenavista Hotel and Restaurant exclusively for you.\r\n\r\nWiFi is available within public areas of the property to help you to stay connected with family and friends.\r\n\r\nVillas Buenavista Hotel and Restaurant is a hotel with great comfort and excellent service according to most hotel\'s guests.\r\n\r\nVillas Buenavista Hotel and Restaurant is a wise choice for travelers visiting Urbiztondo.','app'),
(11,12,'La Union Blue Marlin Beach Resort','Money','Drake','1989-12-20','La Union Blue Marlin Beach Resort','+63728887777','566-566-456-5',NULL,0,'This hotel is the best spot for you who desire a serene and peaceful getaway, far away from the crowds.\r\n\r\nLa Union Blue Marlin Beach Resort is the smartest choice for you who are looking for affordable accommodation with outstanding service.\r\n\r\nSavor your favorite dishes with special cuisines from La Union Blue Marlin Beach Resort exclusively for you.\r\n\r\nWiFi is available within public areas of the property to help you to stay connected with family and friends.\r\n\r\nLa Union Blue Marlin Beach Resort is a hotel with great comfort and excellent service according to most hotel\'s guests.\r\n\r\nStaying at La Union Blue Marlin Beach Resort will surely satisfy you with its great hospitality and affordable price.','app'),
(12,13,'Chill\'n Hotel','Gray','Fulbuster','1989-12-20','Chill\'n Hotel, san juan, la union, La Union','+63728887777','566-566-456-5','https://www.facebook.com/chillnhotel/?ref=hl',0,'This hotel is the best spot for you who desire a serene and peaceful getaway, far away from the crowds.\r\n\r\nChill\'n Hotel is the smartest choice for you who are looking for affordable accommodation with outstanding service.\r\n\r\nHave an enjoyable and relaxing day at the pool, whether you’re traveling solo or with your loved ones.\r\n\r\nWiFi is available within public areas of the property to help you to stay connected with family and friends.\r\n\r\nChill\'n Hotel is a hotel with great comfort and excellent service according to most hotel\'s guests.\r\n\r\nChill\'n Hotel is the ideal choice for you who are looking for a comfortable yet affordable accommodation.','app'),
(13,14,'Oasis Country Resort','Taylor','Swift','1989-12-20','Oasis Country Resort Hotel, San Fernando, La Union','+63728887777','566-566-456-5','http://www.oasiscountryresort.com/',0,'Have an enjoyable and relaxing day at the pool, whether you’re traveling solo or with your loved ones.\r\n\r\nGet the best deal for finest quality of spa treatment to unwind and rejuvenate yourself.\r\n\r\n24-hours front desk is available to serve you, from check-in to check-out, or any assistance you need. Should you desire more, do not hesitate to ask the front desk, we are always ready to accommodate you.\r\n\r\nSavor your favorite dishes with special cuisines from Oasis Country Resort exclusively for you.\r\n\r\nWiFi is available within public areas of the property to help you to stay connected with family and friends.\r\n\r\nOasis Country Resort is a resort with great comfort and excellent service according to most resort\'s guests.\r\n\r\nWith all facilities offered, Oasis Country Resort is the right place to stay.','app'),
(14,15,'Sea of Dreams Resort','Natsu','Dragneel','1989-12-20','Sea of Dreams Resort - Spa, Caba, ABR','+63728887777','566-566-456-5',NULL,0,'The resort’s fitness center is a must-try during your stay here.\r\n\r\nHave an enjoyable and relaxing day at the pool, whether you’re traveling solo or with your loved ones.\r\n\r\nGet the best deal for finest quality of spa treatment to unwind and rejuvenate yourself.\r\n\r\nSavor your favorite dishes with special cuisines from Sea of Dreams Resort - Spa exclusively for you.\r\n\r\nWiFi is available within public areas of the property to help you to stay connected with family and friends.\r\n\r\nWith all facilities offered, Sea of Dreams Resort - Spa is the right place to stay.','app');

/*Table structure for table `photos` */

DROP TABLE IF EXISTS `photos`;

CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `location` text,
  PRIMARY KEY (`id`),
  KEY `photo_to_room` (`room_id`),
  CONSTRAINT `photo_to_room` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `photos` */

/*Table structure for table `pnumbers` */

DROP TABLE IF EXISTS `pnumbers`;

CREATE TABLE `pnumbers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `number` varchar(15) DEFAULT NULL,
  `token` text,
  PRIMARY KEY (`id`),
  KEY `token_to_account` (`uid`),
  CONSTRAINT `token_to_account` FOREIGN KEY (`uid`) REFERENCES `users_account` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pnumbers` */

/*Table structure for table `rating` */

DROP TABLE IF EXISTS `rating`;

CREATE TABLE `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `clean` decimal(1,1) DEFAULT NULL,
  `service` decimal(1,1) DEFAULT NULL,
  `location` decimal(1,1) DEFAULT NULL,
  `rooms` decimal(1,1) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `rating_to_hotelier` (`uid`),
  CONSTRAINT `rating_to_hotelier` FOREIGN KEY (`uid`) REFERENCES `hotelier_profile` (`uid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rating` */

/*Table structure for table `reservations` */

DROP TABLE IF EXISTS `reservations`;

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) DEFAULT NULL,
  `traveler_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `check_in` date DEFAULT NULL,
  `check_out` date DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_to_traveler` (`traveler_id`),
  KEY `reservation_to_hotel` (`hotel_id`),
  KEY `reservation_to_room` (`room_id`),
  CONSTRAINT `reservation_to_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotelier_profile` (`uid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `reservation_to_room` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `reservation_to_traveler` FOREIGN KEY (`traveler_id`) REFERENCES `traveler_profile` (`uid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reservations` */

/*Table structure for table `rooms` */

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `units` int(11) DEFAULT NULL,
  `max_children` int(11) DEFAULT NULL,
  `max_adult` int(11) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `description` text,
  `price_per_night` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_to_hotelier` (`uid`),
  CONSTRAINT `rooms_to_hotelier` FOREIGN KEY (`uid`) REFERENCES `hotelier_profile` (`uid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rooms` */

/*Table structure for table `traveler_profile` */

DROP TABLE IF EXISTS `traveler_profile`;

CREATE TABLE `traveler_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_to_account` (`uid`),
  CONSTRAINT `profile_to_account` FOREIGN KEY (`uid`) REFERENCES `users_account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `traveler_profile` */

insert  into `traveler_profile`(`id`,`uid`,`firstname`,`lastname`,`birthdate`,`address`,`mobile`) values 
(1,1,'Renz Carlo','Salanga','1998-11-10','Canaoay, City of San Fernando La Union','9662797237');

/*Table structure for table `users_account` */

DROP TABLE IF EXISTS `users_account`;

CREATE TABLE `users_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `activated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `users_account` */

insert  into `users_account`(`id`,`email`,`password`,`type`,`activated`) values 
(1,'thkingrenz23@gmail.com','salangalich','traveler',0),
(2,'thunderbird23@gmail.com','salangalich','hotelier',0),
(3,'constavilla23@gmail.com','salangalich','hotelier',0),
(4,'larocavilla23@gmail.com','salangalich','hotelier',0),
(5,'marandresort23@gmail.com','salangalich','hotelier',0),
(6,'maritonibali23@gmail.com','salangalich','hotelier',0),
(7,'aureoresort23@gmail.com','salangalich','hotelier',0),
(8,'woodsway23@gmail.com','salangalich','hotelier',0),
(9,'cozycabin23@gmail.com','salangalich','hotelier',0),
(10,'goldensunrise23@gmail.com','salangalich','hotelier',0),
(11,'buenavistavilla23@gmail.com','salangalich','hotelier',0),
(12,'bluemarlin23@gmail.com','salangalich','hotelier',0),
(13,'chillnhotel23@gmail.com','salangalich','hotelier',0),
(14,'oasis23@gmail.com','salangalich','hotelier',0),
(15,'seaofdreams23@gmail.com','salangalich','hotelier',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
