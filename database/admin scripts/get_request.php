<?php

	/*Declare all requirements*/
    header('Access-Control-Allow-Origin: *');
    require '../../database.php';
    
    /*get all paramaters data*/
    
	
	/*Create a database connection*/
    $wire = new db();
    
    /*Create Query*/
    $qry = "SELECT account.id, account.activated, hotel.name,hotel_owner.firstname, hotel_owner.lastname, location.address, hotel.mobile_no FROM account , hotel, location, hotel_owner WHERE account.id = hotel.id and hotel_owner.hotel_id = account.id and location.hotel_id = account.id and account.type = 'hotelier'";
    
    /*get data from the database*/
    $data = $wire->get_data($qry);
	
	echo json_encode($data);