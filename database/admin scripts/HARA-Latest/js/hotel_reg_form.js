$("#submit").click(function(){
	load("Loading..");
	
	var hotel_name = $('#hotel_name').val();
	var owner_firstname = $('#owner_firstname').val();
	var owner_lastname = $('#owner_lastname').val();
	var owner_bdate = $('#owner_bdate').val();
	var owner_address = $('#owner_address').val();
	var hotel_mnumber = $('#hotel_mnumber').val();
	var hotel_tnumber = $('#hotel_tnumber').val();
	var hotel_email = $('#hotel_email').val();
	var password = $('#password').val();
	var cpassword = $('#cpassword').val();
	var domain = $('#domain').val();
	var isForward = $('#isForward').val();
	
	if(isEquals(password,cpassword)){
		if(isFilled([hotel_name, owner_firstname, owner_lastname, owner_bdate, owner_address, hotel_mnumber, hotel_tnumber, hotel_email, password, cpassword, domain, isForward])){
			$.ajax({
				type: 'POST',
				url: 'http://'+server_ip+'/HARA/database/register_hotelier.php',
				data:{
					hotel_name : hotel_name,
					owner_firstname : owner_firstname,
					owner_lastname : owner_lastname,
					owner_bdate : owner_bdate,
					owner_address: owner_address,
					hotel_mnumber : hotel_mnumber,
					hotel_tnumber: hotel_tnumber,
					hotel_email: hotel_email,
					password: password,
					domain: domain,
					isForward : isForward
				},
				dataType: 'json'
			}).done(function(response){
				if(response.account == true && response.profile == true){
					stop_load();
					toastF("Success");
					window.location="index.html";
				}else{
					stop_load();
					toastF("Failed");
				}
			});
		}else{
			stop_load();
			toastF("Pls. Fill all Fields");
		}
	}else{
		stop_load();
		toastF("Password not match");
	}
});


