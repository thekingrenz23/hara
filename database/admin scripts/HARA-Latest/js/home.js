
var options = {
    date: new Date(),
    mode: 'date', // or 'time'
    minDate: new Date() - 10000,
	androidTheme: 3
  };
  var inDate = new Date();
  var outOption = {
	date: new Date(),
    mode: 'date', // or 'time'
    minDate: inDate - 10000,
	androidTheme: 3
  }

var speechOption = {
  matches: 1,
  prompt: "HARA",     // Android only
  showPopup: true  // Android only
}  
 
 

/*--------------------------------------------------------------------------------------------------------------------*/
//Check in button
$('#check-in').click(function(e){
	
	datePicker.show(options, function(date){
		//alert(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDate());
		$('#in-date').text(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDate());
		var crap = date.setTime(date.getTime() + (86400000*2));
		var baro = new Date(crap) - 10000;
		outOption.minDate = baro;
		$('#check-out').html("<i class='material-icons'>date_range</i>	<small id='out-date'>Check-out</small>");
	}, function(error){
		
	});
});
/*--------------------------------------------------------------------------------------------------------------------*/
//Check out Button
$('#check-out').click(function(){
	
	datePicker.show(outOption, function(date){
		//alert(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDate());
		
		$('#out-date').text(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDate());
	}, function(error){
		
	});
});
/*--------------------------------------------------------------------------------------------------------------------*/
//Location Button
$('#location').focus(function(){
	$('#search-card').addClass("z-depth-5");
});

$('#location').focusout(function(){
	$('#search-card').removeClass("z-depth-5");
});
/*--------------------------------------------------------------------------------------------------------------------*/
//Guest Control Add and Subtract btn
var children=0;
var adult =1;

$('#adult').text(adult);
$('#children').text(children);

//Button up
$('#up-adult').click(function(){
	adult = parseInt($('#adult').text());
	adult+=1;
	$('#adult').text(adult);
});
$('#up-children').click(function(){
	children = parseInt($('#children').text());
	children +=1;
	$('#children').text(children);
});

//Button down
$('#down-adult').click(function(){
	adult = parseInt($('#adult').text());
	if(adult > 0){
		adult-=1;
		$('#adult').text(adult);
	}
});
$('#down-children').click(function(){
	children = parseInt($('#children').text());
	if(children > 0){
		children-=1;
		$('#children').text(children);
	}
});
$('#save-guest').click(function(){
	$('#guest-btn').html("<i class='material-icons'>person</i><small id='guest-number'>"+(adult+children) + "</small>");
});
/*--------------------------------------------------------------------------------------------------------------------*/
//Search Button
$('#search-btn').click(function(){
	//window.location = "list-view.html";
	var location = $('#location').val();
	var checkInDate = $('#in-date').text();
	var checkOutDate = $('#out-date').text();
	var guest = $('#guest-number').text();
	//Check if the Fields is okay
	
	if(location.replace(/\s/g, '').length > 0 && checkInDate.replace(/\s/g, '').length > 0 && checkOutDate.replace(/\s/g, '').length > 0 && checkInDate != "Check-in" && checkOutDate != "Check-out" && parseInt(guest) > 0){
		window.location="list-view.html";
	}else{
		window.plugins.toast.showWithOptions(
			{
			  message: "Please Fill All Fields",
			  duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
			  position: "bottom",
			  addPixelsY: -40  // added a negative value to move it up a bit (default 0)
			}
		);
	}
	
});
/*--------------------------------------------------------------------------------------------------------------------*/
//Mic Button
$('#mic').click(function(){
	window.plugins.speechRecognition.isRecognitionAvailable(micOkay, micError);
});

function micOkay(answer){
	if(answer){
		window.plugins.speechRecognition.hasPermission(micPermission, errorMicPermission);
	}else{
		alert("Speech Recognition not available");
	}
}

function micError(message){
	alert(message);
}

function micPermission(answer){
	if(answer){
		window.plugins.speechRecognition.startListening(userSpeech, errorUserSpeech, speechOption);
	}else{
		getPermission();
	}
}

function errorMicPermission(message){
	alert(message);
}

function getPermission(){
	window.plugins.speechRecognition.requestPermission(permissionCallback, permissionErrorCallback);
}

function permissionCallback(){
	micPermission();
}

function permissionErrorCallback(){
	getPermission();
}

function userSpeech(message){
	alert(message);
}

function errorUserSpeech(message){
	alert(message);
}












