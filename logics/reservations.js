var reservations = {
    init: function(){
        $(document).ready(this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        this.getPending(107);

        

    },
    onBack: function(e){
        e.preventDefault();
    },
    getPending: function(id){
        
        $.ajax({
            url: 'http://'+server_ip+'/HARA/database/getPendingRequest.php',
            type: 'GET',
            data: {
                id:id
            },
            dataType: 'json',
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert('Error - ' + errorMessage);
            }
        }).done(function(data){
            if(data.requests.result == "filled"){
                for(var i=0; i<data.requests.items.length; i++){
                    if(data.requests.items[i].status == 0){
                        $('#pending-list').append("<li class='collection-item avatar'> <img src='https://team3gmanager.000webhostapp.com/HARA/images/img/"+data.requests.items[i].photos.items[0].location+"' alt='' class='circle'> <span class='title'>"+data.requests.items[i].name+"</span> <p class='address'>"+data.requests.items[i].address+"<br> <b class='date'>"+data.requests.items[i].check_in +" "+ data.requests.items[i].check_out +"</b> </p> <a class='waves-effect waves-light btn block' onclick='can()' href='#'> cancel reservation </a> </li>");
                    }else if(data.requests.items[i].status == 1){
                        $('#succeed-list').append("<li class='collection-item avatar'> <a onclick=\"con('"+data.requests.items[i].idiot+"','"+data.requests.items[i].address+"','"+data.requests.items[i].id+"')\" name='"+data.requests.items[i].address+"' style='color: black'><img src='https://team3gmanager.000webhostapp.com/HARA/images/img/"+data.requests.items[i].photos.items[0].location+"' alt='' class='circle'> <span class='title'>"+data.requests.items[i].name+"<i class='material-icons right'>check_circle</i></span> <p>"+data.requests.items[i].address+"<br> "+data.requests.items[i].check_in +" "+ data.requests.items[i].check_out +" </p> </a> </li>");
                    }else{
                        $('#ido').hide();
                    }
                }


                $('#loader').hide();
                $('#loader-1').hide();
                $('#loader-2').hide();
            }else{
                $('#loader').hide();
            }
        });
    }
}
reservations.init();