$(document).on('deviceready', function(){
	//StatusBar.backgroundColorByHexString("#ffffff");
	StatusBar.styleDefault();
});

$(document).on('backbutton', function(e){
	e.preventDefault();
});


//Signup Button
$('#sign-up').click(function(){
	window.location = "register.html";
});

//Login Button
$('#log-in').click(function(){
	
	
	load("Logging in..");
	
	var pass = $('#pass').val();
	var email = $('#email').val();
	
    if(isFilled([pass,email])){
		$.ajax({
			type: 'POST',
			url: 'http://'+server_ip+'/HARA/database/auth_user.php',
			data: {
				email: email,
				password: pass
			},
			dataType: 'json'
		}).done(function(response){
			if(response.legit == true){
				if(response.type == "traveler"){
					if(response.activated == true){
						localStorage.setItem('uid',response.id);
						stop_load();
						window.location = "home.html";
					}else{
						localStorage.setItem('uid',response.id);
						stop_load();
						window.location = "login-auth-email.html";
					}
				}else if(response.type == "hotelier"){
					if(response.activated == true){
						localStorage.setItem('uid',response.id);
						$.ajax({
							type: 'POST',
							url: 'http://'+server_ip+'/HARA/database/forward_list.php',
							data:{
								email: email
							},
							dataType: 'json'
						}).done(function(response){
							if(response.message == "success"){
								if(response.forward == "1"){
									stop_load();
									window.location = "web-hotel-home.html";
								}else{
									stop_load();
									window.location = "noweb-hotel-home.html";
								}
							}else{
									stop_load();
									toastF("Email not Found");
							}
						});
					}else{
						$.ajax({
							url: 'http://'+server_ip+'/HARA/database/accepted.php',
							type: 'POST',
							dataType: 'json',
							data: {
								uid: response.id
							}
						}).done(function(data){
							if(data.forward == 2){
								stop_load();
								window.location = "https://developer.globelabs.com.ph/dialog/oauth/qqGAfdgRKpu4RioRpEcRaxuGzq48fAoG/";
							}else if(data.forward == 3){
								stop_load();
								toastF("Wait For Admin to Comfirm your Hotel!");
							}else{
								localStorage.setItem('uid',response.id);
								stop_load();
								window.location = "login-auth-email.html";
							}
						});
					}	
				}else if(response.type == "fd"){
					localStorage.setItem('uid',response.id);
					stop_load();
					window.location="fd-hotel-home.html";
				}
			}else{
				stop_load();
				toastF("Wrong Email/Password");
			}
		});
	}else{
		stop_load();
		toastF("Pls. Fill all Fields");
	}
	
});

//Forgot Password Button
$('#forgot-password').click(function(){
	toastF("Han ko pay naaramid datuy");
});
