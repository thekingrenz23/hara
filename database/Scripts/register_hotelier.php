<?php 	
	/*Declare all requirements*/
    header('Access-Control-Allow-Origin: *');
    require '../../database.php';

	/*POST all parameters*/
	$hotel_name = $_GET['hotel_name'];
	$owner_firstname = $_GET['owner_firstname'];
	$owner_lastname = $_GET['owner_lastname'];
	$owner_bdate = $_GET['owner_bdate'];
	$owner_address = $_GET['owner_address'];
	$hotel_mnumber = $_GET['hotel_mnumber'];
	$hotel_tnumber = $_GET['hotel_tnumber'];
	$hotel_email = $_GET['hotel_email'];
	$password = $_GET['password'];
	$domain = $_GET['domain'];
	$isForward = $_GET['isForward'];
	$lat = $_GET['lat'];
	$lng = $_GET['lng'];
	$fd_email = $_GET['fd_email'];
	$fd_password = $_GET['fd_password'];
	
	/*Create a databse connection*/
	$wire = new db();
	$num = rand(1000,9999);	

	/*Create a query*/ 	
	$qry = "INSERT INTO account VALUES(null, '$hotel_email', '$password', 'hotelier', 0,$num)";

	/*insert registration*/
	$data['account'] = $wire->manipulate_data($qry, "account");

	$last_id = $wire->id;
	$data['id'] = $last_id;

	$qry = "INSERT INTO hotel VALUES($last_id, '$hotel_name', '9$hotel_mnumber', '$hotel_tnumber', '$domain', $isForward, null)";

	/*insert to profile*/
	$data['profile'] =$wire->manipulate_data($qry, "hotel");

	/*Insert hotel owner*/
	$qry = "INSERT INTO hotel_owner VALUES($last_id, '$owner_firstname', '$owner_lastname', '$owner_bdate')";
	
	$data['onwer'] =$wire->manipulate_data($qry, "hotel_owner");
	
	/*Insert hotel location*/
	$qry = "INSERT INTO location VALUES($last_id, '$owner_address', $lat, $lng)";
	
	$data['location'] =$wire->manipulate_data($qry, "location");

	$qry = "INSERT INTO frontdesk VALUES(null, $last_id, '$fd_email', '$fd_password')";

	$data['fd'] =$wire->manipulate_data($qry, "frontdesk");
	
	echo json_encode($data);
