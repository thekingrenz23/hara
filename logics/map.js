var map = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        var data = JSON.parse(localStorage.getItem('mapdata'));
        var mymap = L.map('map').setView([data.hotels.items[0].latitude, data.hotels.items[0].longitude], 13);
		
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 19,
			attribution: 'HARA &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(mymap);
		
     
        var marker = [];
        
        for(var i=0; i<data.hotels.items.length; i++){
            var hotel_name = data.hotels.items[i].name;
            var best_deal = data.hotels.items[i].best_deal;
            var lat = data.hotels.items[i].latitude;
            var lng = data.hotels.items[i].longitude;
            var id = data.hotels.items[i].hotel_id;

            var content = `<h5>${hotel_name}</h5><hr><b>Best Deal: Php ${best_deal}</b><hr><a class="waves-effect waves-light btn white bk" href="javascript:yeah('${id}')">view hotel</a>`;

            marker[i] = L.marker([lat, lng]).addTo(mymap);
            marker[i].bindPopup(content).openPopup();
        }

        var web_marker = [];

        for(var x=0; x<data.hotels_web.items.length; x++){
            var hotel_name = data.hotels_web.items[x].name;
            var lat = data.hotels_web.items[x].latitude;
            var lng = data.hotels_web.items[x].longitude;
            var domain = data.hotels_web.items[x].domain;
            
            var content = `<h5>${hotel_name}</h5><hr><b>ADVERTISEMENT</b><hr><a class="waves-effect waves-light btn white bk" href="javascript:go('${domain}')">VIEW WEBSITE</a>`;

            web_marker[x] = L.marker([lat, lng]).addTo(mymap);
            web_marker[x].bindPopup(content).openPopup();
        }
    },
    onBack: function(e){
        e.preventDefault();
    }
}   
map.init();