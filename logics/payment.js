var payment = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        load("Calculating Price..");
        var room = JSON.parse(localStorage.getItem("room"));
		var optional = JSON.parse(localStorage.getItem("want_am"));
        
        var total = 0;

    
        for(var i=0; i<optional.length; i++){
            total+=parseFloat(optional[i].price);
        }

        total+=parseFloat(room.price);

        stop_load();

        $('#total-price').text("Total Price: Php "+total);

        $('#book').click(function(){
            SpinnerPlugin.activityStart("Processing...", options);
            var criteria = JSON.parse(localStorage.getItem("criteria"));
            
            var cin = criteria.checkin.split('/').join('-');
            var cout = criteria.checkout.split('/').join('-');

            $.ajax({
                url: 'http://'+server_ip+'/HARA/database/bookRoom.php',
                type: 'GET',
                data: {
                    room_id: room.room_id,
                    t_id: localStorage.getItem('uid'),
                    check_in: cin,
                    check_out: cout,
                    total: total,
                    optional: JSON.stringify(optional)
                },
                dataType: 'json',
                error: function(xhr, status, error){
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    alert('Error - ' + errorMessage);
                }
            }).done(function(data){
                if(data.reservation == true && data.suck == true){
                    stop_load();
                    toastF("Success!");
                    window.location="reservations.html";
                }else{
                    stop_load();
                    toastF("Slow Internet");
                }
            });
            
            /*setTimeout(function(){
                SpinnerPlugin.activityStop();
                window.location = "reservation-message.html";
            },3000);*/
        });
    },
    onBack: function(e){
        e.preventDefault();
    },
    calculatePrice: function(room_id){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA',
            type: 'GET',
            data: {
                
            },
            dataType: 'json'
        })
    }
}
payment.init();