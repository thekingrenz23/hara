<?php 	
	/*Declare all requirements*/
    header('Access-Control-Allow-Origin: *');
    require '../../database.php';

	/*POST all parameters*/
	$hotel_name = $_POST['hotel_name'];
	$owner_firstname = $_POST['owner_firstname'];
	$owner_lastname = $_POST['owner_lastname'];
	$owner_bdate = $_POST['owner_bdate'];
	$owner_address = $_POST['owner_address'];
	$hotel_mnumber = $_POST['hotel_mnumber'];
	$hotel_tnumber = $_POST['hotel_tnumber'];
	$hotel_email = $_POST['hotel_email'];
	$password = $_POST['password'];
	$domain = $_POST['domain'];
	$isForward = $_POST['isForward'];
	$lat = $_POST['lat'];
	$lng = $_POST['lng'];
	
	/*Create a databse connection*/
	$wire = new db();
	$num = rand(1000,9999);	

	/*Create a query*/ 	
	$qry = "INSERT INTO account VALUES(null, '$hotel_email', '$password', 'hotelier', 0,$num)";

	/*insert registration*/
	$data['account'] = $wire->manipulate_data($qry, "account");

	$last_id = $wire->id;
	$data['id'] = $last_id;

	$qry = "INSERT INTO hotel VALUES($last_id, '$hotel_name', '9$hotel_mnumber', '$hotel_tnumber', '$domain', $isForward, null)";

	/*insert to profile*/
	$data['profile'] =$wire->manipulate_data($qry, "hotel");

	/*Insert hotel owner*/
	$qry = "INSERT INTO hotel_owner VALUES($last_id, '$owner_firstname', '$owner_lastname', '$owner_bdate')";
	
	$data['onwer'] =$wire->manipulate_data($qry, "hotel_owner");
	
	/*Insert hotel location*/
	$qry = "INSERT INTO location VALUES($last_id, '$owner_address', $lat, $lng)";
	
	$data['location'] =$wire->manipulate_data($qry, "location");
	
	echo json_encode($data);
