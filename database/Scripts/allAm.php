<?php
    header('Access-Control-Allow-Origin:*');
    require_once('../../database.php');

    $wire = new db();

    //Get passed data here
    
    $query = "SELECT * FROM amenity_type";

    //Returns the data['result'] = 'filled' | 'empty', data['items'] = [array of items]
    $data=$wire->get_data($query);

    //Returns the true is success | false if failed
    //$data['rq']=$wire->manipulate_data($query, 'table_name');

    echo json_encode($data);