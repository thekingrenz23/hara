var noweb = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        
        //load sidebar data
        $.when(noweb.getUserData(localStorage.getItem("uid"))).then(function(data){
            if(data.account.result == "filled" && data.owner.result == "filled"){
                $('#email').text(data.account.items[0].email);
				$('#name').text(data.owner.items[0].firstname + " " + data.owner.items[0].lastname);
            }else{
                toastF("User data not found!");
            }
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF("Error: "+errorMessage + " " + error);
        });

    },
    onBack: function(e){
        e.preventDefault();
    },
    getUserData: function(uid){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/user_data.php',
            type: 'GET',
            data: {
                uid: uid
            },
            dataType: 'json'
        })
    }
}
noweb.init();