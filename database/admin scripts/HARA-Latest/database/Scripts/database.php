<?php

class db{
	
	public $db;
	
	function __construct(){
		$this->db_connect('localhost','id6392690_renz','salangalich','id6392690_try');
	}
	
	function db_connect($host, $username, $password, $database){
		$this->db = new mysqli($host, $username, $password, $database);
		if($this->db->connect_errno > 0){
			die('Unable to connect to database'. $this->db->connect_error);
		}
	}
	
	function check_changes($table){
		$result = $this->db->query("select * from $table where uid=1");
		if($result = $result->fetch_object()){
			return $result->counter;
		}
		return 0;
	}
	
	function register_changes($table){
		$this->db->query("update $table set counter= counter+1 where uid=1");
	}
	
	function get_data($query){
		if($result = $this->db->query($query)){
			while($item = $result->fetch_object()){
				$data['items'] = $item;
			}

			if(isset($data['items'])){
				$data['result'] = "filled";
			}else{
				$data['result'] = "empty";
			}
			return  $data;
		}
	}
	
	function manipulate_data($query,$table){
		if($this->db->query($query)){
			//$this->register_changes($table);
			return TRUE;
		}
		return FALSE;
	}

	function get_last_id(){
		return $this->db->insert_id;
	}
}

