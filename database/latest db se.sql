-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2019 at 09:15 AM
-- Server version: 10.3.13-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id6392690_try`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `email` varchar(70) DEFAULT NULL,
  `password` varchar(70) DEFAULT NULL,
  `type` varchar(11) DEFAULT NULL,
  `activated` int(11) DEFAULT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `email`, `password`, `type`, `activated`, `code`) VALUES
(34, 'polintangjon@gmail.com', 'salangalich', 'hotelier', 1, 2014),
(43, 'team3gmanager@gmail.com', '12312', 'hotelier', 1, 2561),
(44, 'darylgail01@gmail.com', 'daryl123', 'hotelier', 1, 1861),
(45, 'maryrosepasia09@gmail.com', '010998', 'hotelier', 2, 5333),
(46, 'leoneilwebster8@gmail.com', '09565137881', 'hotelier', 1, 6053),
(47, 'ninaninobla@gmail.com', 'oreo', 'hotelier', 1, 1767),
(48, 'shengcastillo01@gmail.com', 'pass', 'hotelier', 1, 4999),
(49, 'chadmax1220@gmail.com', 'chad121320', 'hotelier', 4, 4974),
(50, 'chadmax1220gmail.com', 'chad121320', 'hotelier', 0, 4003),
(51, 'fabros2190@gmail.com', 'salangalich', 'hotelier', 0, 3141),
(52, 'chadmax808@gmail.com', 'chad121320', 'traveler', 0, 5036),
(53, 'polintangmharckjon@yahoo.com', 'salangalich', 'traveler', 0, 4374);

-- --------------------------------------------------------

--
-- Table structure for table `amenity`
--

CREATE TABLE `amenity` (
  `id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `amenity_id` int(11) DEFAULT NULL,
  `price` decimal(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenity`
--

INSERT INTO `amenity` (`id`, `room_id`, `amenity_id`, `price`) VALUES
(38, 26, 11, 50.00),
(39, 26, 12, 3.00),
(40, 26, 3, NULL),
(41, 26, 6, NULL),
(42, 27, 5, 500.00),
(43, 27, 6, 300.00),
(44, 27, 1, NULL),
(45, 27, 2, NULL),
(54, 30, 9, 5.00),
(55, 30, 10, 3.00),
(56, 30, 3, NULL),
(57, 30, 6, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `amenity_list`
--

CREATE TABLE `amenity_list` (
  `id` int(11) NOT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `amenity_id` int(11) DEFAULT NULL,
  `price` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `amenity_type`
--

CREATE TABLE `amenity_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenity_type`
--

INSERT INTO `amenity_type` (`id`, `name`, `description`) VALUES
(1, 'Bath Towel', 'A soft large towel to help you with your bath 2 item'),
(2, 'Parking', 'Worry no more for a parking space'),
(3, 'TV', 'Watch your favorite series on tv'),
(4, 'Daily housekeeping', 'Let us help you maintain your room'),
(5, 'Table Lamp', 'Work in the night use our lamp to read in night'),
(6, 'CD / DVD Player', 'Play your favorite dvd or cd on your room'),
(7, 'Alarm Clock', 'Wake up in the morning early with our alarm clock'),
(8, 'Coffee Maker', 'Make your own coffee on your room'),
(9, 'Ironing Board', 'Make your outfit more beautiful by using our iron table'),
(10, 'Iron', 'use our iron to make your suite better looking'),
(11, 'Hair dryer', 'Dry your hair with our hair dryer'),
(12, 'Shaving Kit', 'Shave your hair with our shaving kit');

-- --------------------------------------------------------

--
-- Table structure for table `availed_amenity`
--

CREATE TABLE `availed_amenity` (
  `id` int(11) NOT NULL,
  `availed_id` int(11) DEFAULT NULL,
  `amenity_id` int(11) DEFAULT NULL,
  `price` decimal(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `availed_room`
--

CREATE TABLE `availed_room` (
  `id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `traveler_id` int(11) DEFAULT NULL,
  `check_in` date DEFAULT NULL,
  `check_out` date DEFAULT NULL,
  `total_price` decimal(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `average_rating`
--

CREATE TABLE `average_rating` (
  `hotel_id` int(11) NOT NULL,
  `cleanliness` decimal(1,1) DEFAULT NULL,
  `service` decimal(1,1) DEFAULT NULL,
  `location` decimal(1,1) DEFAULT NULL,
  `rooms` decimal(1,1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bed`
--

CREATE TABLE `bed` (
  `id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `bed_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bed`
--

INSERT INTO `bed` (`id`, `room_id`, `bed_id`) VALUES
(19, 26, 2),
(20, 26, 3),
(21, 27, 3),
(22, 27, 4),
(27, 30, 3),
(28, 30, 4);

-- --------------------------------------------------------

--
-- Table structure for table `bed_type`
--

CREATE TABLE `bed_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bed_type`
--

INSERT INTO `bed_type` (`id`, `name`, `capacity`) VALUES
(1, 'Single/Twin bed', 1),
(2, 'Full bed/Double beds', 2),
(3, 'Queen bed', 2),
(4, 'King bed', 3);

-- --------------------------------------------------------

--
-- Table structure for table `cover_photo`
--

CREATE TABLE `cover_photo` (
  `int` int(11) NOT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `location` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `frontdesk`
--

CREATE TABLE `frontdesk` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `frontdesk`
--

INSERT INTO `frontdesk` (`id`, `hotel_id`, `email`, `password`) VALUES
(1, 43, 'farmer23@gmail.com', '12312');

-- --------------------------------------------------------

--
-- Table structure for table `globe_token`
--

CREATE TABLE `globe_token` (
  `number` varchar(15) NOT NULL,
  `token` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `globe_token`
--

INSERT INTO `globe_token` (`number`, `token`) VALUES
('9662797237', 'AZh0VV1L0sw5qj91V2bYxwbVQ9UP-rv-9HZr-YiGx3M');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `telephone_no` varchar(20) DEFAULT NULL,
  `domain` text DEFAULT NULL,
  `forward` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `mobile_no`, `telephone_no`, `domain`, `forward`, `description`) VALUES
(34, 'Sunshine', '9271552145', '0000', 'sunshinehotel.com', 0, NULL),
(43, 'Go inn', '9662797237', '23213123', 'Gg.com', 0, NULL),
(44, 'Go out', '9661737476', '23232323', 'Goout.com', 1, NULL),
(45, 'Go Inn', '9192616304', '0756986304', 'goinn.com', 0, NULL),
(46, 'SOGood', '9565137881', '14324', 'SOGood.com', 1, NULL),
(47, 'Transilvaniab hotel', '9361336074', '7004', 'Transilvania.com', 1, NULL),
(48, 'Hotel 65', '9059392430', '0756264', 'Hotel65.com', 1, NULL),
(49, 'Hotel 44', '9272857895', '2421132', 'Hotel 44.com', 1, NULL),
(50, '45HOTEL', '9272857895', '2421138', '45hotel.com', 1, NULL),
(51, 'DarkNight', '9271552145', '0000', 'darknight.com', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_owner`
--

CREATE TABLE `hotel_owner` (
  `hotel_id` int(11) NOT NULL,
  `firstname` varchar(70) DEFAULT NULL,
  `lastname` varchar(70) DEFAULT NULL,
  `bdate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel_owner`
--

INSERT INTO `hotel_owner` (`hotel_id`, `firstname`, `lastname`, `bdate`) VALUES
(34, 'Mharck Jon', 'Polintang', '2019-01-22'),
(43, 'Renz', 'Salanga', '2019-02-05'),
(44, 'Daryl Jan', 'Catapang', '2019-02-06'),
(45, 'Mary Rose', 'Pasia', '2019-02-06'),
(46, 'Webster', 'Lionoel', '1998-11-02'),
(47, 'Nina Leen', 'Ninobla', '2019-02-06'),
(48, 'Roselyn Mae', 'Castillo', '2019-02-06'),
(49, 'Richard', 'Pacio', '2019-02-04'),
(50, 'Richard', 'Pacio', '2019-02-11'),
(51, 'Michael', 'Fabros', '2019-02-17');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `hotel_id` int(11) NOT NULL,
  `address` text DEFAULT NULL,
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`hotel_id`, `address`, `latitude`, `longitude`) VALUES
(34, 'Florentino St, San Fernando, 2500 La Union, Philippines', 16.60905960, 120.31168080),
(43, 'Canaoay City Of San Fernando La Union', 16.58480500, 120.30174530),
(44, '575 Quezon Ave, San Fernando, La Union, Philippines', 16.60661980, 120.31688980),
(45, 'Quezon Ave, San Fernando, La Union, Philippines', 16.60377450, 120.31814460),
(46, '204 Mc Arthur Hwy, San Fernando, 2500 La Union, Philippines', 16.60677190, 120.31724330),
(47, '204 Mc Arthur Hwy, San Fernando, 2500 La Union, Philippines', 16.60682470, 120.31734540),
(48, '575 Quezon Ave, San Fernando, La Union, Philippines', 16.60672220, 120.31701670),
(49, 'San Fernando la union', 16.60737690, 120.36660990),
(50, 'St Joseph Village, San Fernando, La Union, Philippines', 16.60807680, 120.31598220),
(51, 'Catbangen, City of San Fernando, La Union', 16.60669650, 120.31327920);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `date` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `message_id` text DEFAULT NULL,
  `message_text` text DEFAULT NULL,
  `sender_address` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `date`, `address`, `message_id`, `message_text`, `sender_address`) VALUES
(1, 'Fri Jan 18 2019 12:52:26 GMT+0000 (UTC)', 'tel:21588508', '5c41cc0aca30100100b4ab36', 'yes', 'tel:+639662797237'),
(2, 'Fri Jan 18 2019 14:22:07 GMT+0000 (UTC)', 'tel:21588508', '5c41e10fceacdb0100a61a20', 'yes', 'tel:+639662797237'),
(3, 'Sun Jan 20 2019 01:33:53 GMT+0000 (UTC)', 'tel:21588508', '5c43d0017871f101001713b2', 'YES', 'tel:+639662797237'),
(4, 'Sun Jan 20 2019 01:45:39 GMT+0000 (UTC)', 'tel:21588508', '5c43d2c32db3e401004f5f1b', 'yes', 'tel:+639662797237'),
(5, 'Sun Jan 20 2019 01:47:32 GMT+0000 (UTC)', 'tel:21588508', '5c43d334c2d4eb0100d0c4be', 'yes', 'tel:+639662797237'),
(6, 'Sun Jan 20 2019 01:50:39 GMT+0000 (UTC)', 'tel:21588508', '5c43d3efca30100100b56f27', 'yes', 'tel:+639662797237'),
(7, 'Sun Jan 20 2019 01:53:32 GMT+0000 (UTC)', 'tel:21588508', '5c43d49cc2d4eb0100d0c55d', 'yes', 'tel:+639662797237'),
(8, 'Mon Jan 21 2019 16:21:15 GMT+0000 (UTC)', 'tel:21588508', '5c45f17b8c75a50100454d08', 'yes', 'tel:+639662797237'),
(9, 'Mon Jan 21 2019 16:38:36 GMT+0000 (UTC)', 'tel:21588508', '5c45f58cbff1560100a437c1', 'yes', 'tel:+639662797237'),
(10, 'Tue Jan 22 2019 12:16:20 GMT+0000 (UTC)', 'tel:21588508', '5c47099445552001004b418e', 'YES', 'tel:+639271552145'),
(11, 'Sun Feb 03 2019 10:54:55 GMT+0000 (UTC)', 'tel:21588508', '5c56c87f4c5d840100c296f5', 'yes', 'tel:+639662797237'),
(12, 'Mon Feb 04 2019 23:21:23 GMT+0000 (UTC)', 'tel:21588508', '5c58c8f33595310100c17bd5', 'yes', 'tel:+639662797237'),
(13, 'Tue Feb 05 2019 00:42:39 GMT+0000 (UTC)', 'tel:21588508', '5c58dbff3595310100c1a329', 'yes', 'tel:+639662797237'),
(14, 'Tue Feb 05 2019 09:31:36 GMT+0000 (UTC)', 'tel:21588508', '5c5957f8e7ac95010026b48d', 'yes', 'tel:+639662797237'),
(15, 'Tue Feb 05 2019 12:20:52 GMT+0000 (UTC)', 'tel:21588508', '5c597fa48317c101009be210', 'yes', 'tel:+639662797237'),
(16, 'Wed Feb 06 2019 00:55:54 GMT+0000 (UTC)', 'tel:21588508', '5c5a309a03005801002bde16', 'YES', 'tel:+639661737476'),
(17, 'Wed Feb 06 2019 04:16:05 GMT+0000 (UTC)', 'tel:21588508', '5c5a5f8570f3630100548226', 'YES', 'tel:+639565137881'),
(18, 'Wed Feb 06 2019 04:18:01 GMT+0000 (UTC)', 'tel:21588508', '5c5a5ff991a2d80100e83199', 'YES', 'tel:+639361336074'),
(19, 'Wed Feb 06 2019 05:22:34 GMT+0000 (UTC)', 'tel:21588508', '5c5a6f1a8317c101009c7682', 'YES', 'tel:+639059392430'),
(20, 'Fri Mar 15 2019 07:57:51 GMT+0000 (UTC)', 'tel:21589545', '5c8b5aff287f1701006e56f1', 'yes', 'tel:+639662797237');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `location` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `room_id`, `location`) VALUES
(6, 26, '1549369342.jpg'),
(7, 26, '1549369343.jpg'),
(8, 27, '1549369840.jpg'),
(9, 27, '1549369842.jpg'),
(10, 30, '1549370836.jpg'),
(11, 30, '1549370840.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `traveler_id` int(11) DEFAULT NULL,
  `cleanliness` decimal(1,1) DEFAULT NULL,
  `service` decimal(1,1) DEFAULT NULL,
  `location` decimal(1,1) DEFAULT NULL,
  `rooms` decimal(1,1) DEFAULT NULL,
  `comment` decimal(1,1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `traveler_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `check_in` date DEFAULT NULL,
  `check_out` date DEFAULT NULL,
  `total_price` decimal(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `units` int(11) DEFAULT NULL,
  `max_children` int(11) DEFAULT NULL,
  `max_adult` int(11) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `refundable` int(11) DEFAULT NULL,
  `price_per_night` decimal(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `hotel_id`, `name`, `units`, `max_children`, `max_adult`, `size`, `description`, `refundable`, `price_per_night`) VALUES
(26, 43, 'Great Room', 53, 5, 3, '500', NULL, 1, 300.50),
(27, 43, 'Farmer Room', 150, 1, 1, '500', NULL, 1, 20.00),
(30, 43, 'Duxk room', 58, 1, 2, '500', NULL, 1, 300.30);

-- --------------------------------------------------------

--
-- Table structure for table `traveler`
--

CREATE TABLE `traveler` (
  `id` int(11) NOT NULL,
  `firstname` varchar(70) DEFAULT NULL,
  `lastname` varchar(70) DEFAULT NULL,
  `bdate` date DEFAULT NULL,
  `address` text DEFAULT NULL,
  `mobile_no` varchar(15) DEFAULT NULL,
  `visits` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `updater`
--

CREATE TABLE `updater` (
  `table` varchar(150) NOT NULL,
  `counter` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `updater`
--

INSERT INTO `updater` (`table`, `counter`) VALUES
('account', 82),
('amenity', 33),
('bed', 54),
('frontdesk', 2),
('globe_token', 18),
('hotel', 17),
('hotel_owner', 17),
('location', 15),
('photo', 12),
('room', 27),
('traveler', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amenity`
--
ALTER TABLE `amenity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `amenity_to_room` (`room_id`),
  ADD KEY `amenity_to_type` (`amenity_id`);

--
-- Indexes for table `amenity_list`
--
ALTER TABLE `amenity_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `amenity_to_amenity` (`amenity_id`),
  ADD KEY `amenity_to_reservation` (`reservation_id`);

--
-- Indexes for table `amenity_type`
--
ALTER TABLE `amenity_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `availed_amenity`
--
ALTER TABLE `availed_amenity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `av_am_to_avail` (`availed_id`),
  ADD KEY `av_am_to_am_id` (`amenity_id`);

--
-- Indexes for table `availed_room`
--
ALTER TABLE `availed_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `availed_to_traveler` (`traveler_id`),
  ADD KEY `availed_to_room` (`room_id`) USING BTREE;

--
-- Indexes for table `average_rating`
--
ALTER TABLE `average_rating`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `bed`
--
ALTER TABLE `bed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bed_to_room` (`room_id`),
  ADD KEY `bed_to_type` (`bed_id`);

--
-- Indexes for table `bed_type`
--
ALTER TABLE `bed_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cover_photo`
--
ALTER TABLE `cover_photo`
  ADD PRIMARY KEY (`int`),
  ADD KEY `cover_to_hotel` (`hotel_id`);

--
-- Indexes for table `frontdesk`
--
ALTER TABLE `frontdesk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fr_to_hotel` (`hotel_id`);

--
-- Indexes for table `globe_token`
--
ALTER TABLE `globe_token`
  ADD PRIMARY KEY (`number`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_owner`
--
ALTER TABLE `hotel_owner`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phto_to_room` (`room_id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `raitn_to_traveler` (`traveler_id`),
  ADD KEY `rating_to_hotel` (`hotel_id`) USING BTREE;

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservation_to_room` (`room_id`),
  ADD KEY `reservation_to_traveler` (`traveler_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_to_hotel` (`hotel_id`);

--
-- Indexes for table `traveler`
--
ALTER TABLE `traveler`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `updater`
--
ALTER TABLE `updater`
  ADD PRIMARY KEY (`table`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `amenity`
--
ALTER TABLE `amenity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `amenity_type`
--
ALTER TABLE `amenity_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `availed_amenity`
--
ALTER TABLE `availed_amenity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `availed_room`
--
ALTER TABLE `availed_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bed`
--
ALTER TABLE `bed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `bed_type`
--
ALTER TABLE `bed_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cover_photo`
--
ALTER TABLE `cover_photo`
  MODIFY `int` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frontdesk`
--
ALTER TABLE `frontdesk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `amenity`
--
ALTER TABLE `amenity`
  ADD CONSTRAINT `amenity_to_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `amenity_to_type` FOREIGN KEY (`amenity_id`) REFERENCES `amenity_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `amenity_list`
--
ALTER TABLE `amenity_list`
  ADD CONSTRAINT `amenity_to_amenity` FOREIGN KEY (`amenity_id`) REFERENCES `amenity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `amenity_to_reservation` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `availed_amenity`
--
ALTER TABLE `availed_amenity`
  ADD CONSTRAINT `av_am_to_am_id` FOREIGN KEY (`amenity_id`) REFERENCES `amenity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `av_am_to_avail` FOREIGN KEY (`availed_id`) REFERENCES `availed_room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `availed_room`
--
ALTER TABLE `availed_room`
  ADD CONSTRAINT `availed_to_hotel` FOREIGN KEY (`room_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `availed_to_traveler` FOREIGN KEY (`traveler_id`) REFERENCES `traveler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `average_rating`
--
ALTER TABLE `average_rating`
  ADD CONSTRAINT `ave_to_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bed`
--
ALTER TABLE `bed`
  ADD CONSTRAINT `bed_to_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bed_to_type` FOREIGN KEY (`bed_id`) REFERENCES `bed_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cover_photo`
--
ALTER TABLE `cover_photo`
  ADD CONSTRAINT `cover_to_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `frontdesk`
--
ALTER TABLE `frontdesk`
  ADD CONSTRAINT `fr_to_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `hotel_to_account` FOREIGN KEY (`id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel_owner`
--
ALTER TABLE `hotel_owner`
  ADD CONSTRAINT `owner_to_htoel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `location_to_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `phto_to_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `raitn_to_traveler` FOREIGN KEY (`traveler_id`) REFERENCES `traveler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rating_to_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_to_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reservation_to_traveler` FOREIGN KEY (`traveler_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_to_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `traveler`
--
ALTER TABLE `traveler`
  ADD CONSTRAINT `traveler_to_account` FOREIGN KEY (`id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
