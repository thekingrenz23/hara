//Mic Button
$('#mic').click(function(){
	window.plugins.speechRecognition.isRecognitionAvailable(micOkay, micError);
});

function micOkay(answer){
	if(answer){
		window.plugins.speechRecognition.hasPermission(micPermission, errorMicPermission);
	}else{
		alert("Speech Recognition not available");
	}
}

function micError(message){
	alert(message);
}

function micPermission(answer){
	if(answer){
		window.plugins.speechRecognition.startListening(userSpeech, errorUserSpeech, speechOption);
	}else{
		getPermission();
	}
}

function errorMicPermission(message){
	alert(message);
}

function getPermission(){
	window.plugins.speechRecognition.requestPermission(permissionCallback, permissionErrorCallback);
}

function permissionCallback(){
	micPermission();
}

function permissionErrorCallback(){
	getPermission();
}

function userSpeech(message){
	alert(message);
}

function errorUserSpeech(message){
	alert(message);
}