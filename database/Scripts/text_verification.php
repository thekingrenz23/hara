<?php

	/*Declare all requirements*/
    header('Access-Control-Allow-Origin: *');
    require '../../database.php';
    
    /*get all paramaters data*/
    $uid = $_POST['uid'];
	
	/*Create a database connection*/
    $wire = new db();
    
    /*Create Query*/
    $qry = "SELECT account.`id`, hotel.`mobile_no`, globe_token.`token` FROM account, hotel, globe_token WHERE account.`id` = hotel.`id` AND globe_token.`number` = hotel.`mobile_no` AND account.`id`= $uid";
    
    /*get data from the database*/
    $result['data'] = $wire->get_data($qry);
	
	if($result['data']['result'] == "filled"){
		$data['message'] = "success";
		$data['id'] = $result['data']['items'][0]->id;
		$data['number'] = $result['data']['items'][0]->mobile_no;
		$data['token'] = $result['data']['items'][0]->token;
	}else{
		$data['message'] = "error";
		$data['forward'] = 0;
	}
	
	echo json_encode($data);