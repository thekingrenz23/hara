var travelerProfile = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        this.loadTraveler();
    },
    onBack: function(e){
        e.preventDefault();
    },
    loadTraveler: function(){
        $.when(this.getData()).then(function(data){
            
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error - ' + errorMessage);
        });
    },
    getData: function(){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/updateTravelerProfile.php',
            type: 'GET',
            data: {
                uid: localStorage.getItem('uid')
            },
            dataType: 'json'
        })
    }
}
travelerProfile.init();