
var hotel_room_info;
var amenities;
var photos;
var id;
var room_id;
/*------------------------------------------------------------------------*/
$(document).ready(function(){
		
	if(isNull(localStorage.getItem('room_info'))){
		console.log("pls give room info first");
	}else{
		hotel_room_info = JSON.parse(localStorage.getItem('room_info'));
		amenities = JSON.parse(localStorage.getItem('am_price'));
		included_am = JSON.parse(localStorage.getItem('included_am'));
		beds = JSON.parse(localStorage.getItem('beds'));
		id = localStorage.getItem('uid');
		console.log(hotel_room_info);
		console.log(amenities);
		console.log(included_am);
		console.log(beds);
	}

});
/*------------------------------------------------------------------------*/
$(document).on('deviceready',function(){
	load("Loading..");
	$.ajax({
		url: 'http://'+server_ip+'/HARA/database/save_room.php',
		type: "GET",
		data: {
			room_name: hotel_room_info.name,
			units: hotel_room_info.unit,
			price: hotel_room_info.price + "." + hotel_room_info.cents,
			adult: hotel_room_info.adult,
			child: hotel_room_info.child,
			size: hotel_room_info.size,
			uid: id
		},
		dataType: 'json'
	}).done(function(response){
		room_id = response.id;
		
		if(response.room == true){
			toastF("Success");
		}else{
			toastF("Save Room: Failed");
		}
		$.ajax({	
			url: 'http://'+server_ip+'/HARA/database/save_ame.php',
			type: 'GET',
			data:{
				am: amenities,
				id: room_id
			},
			dataType: 'json'
		}).done(function(response){
			
			if(response.status == true){
				toastF("Success");
			}else{
				toastF("Save Optional: Failed");
			}
			$.ajax({
				url: 'http://'+server_ip+'/HARA/database/save_beds.php',
				type: 'GET',
				data: {
					beds: beds,
					id: room_id
				},
				dataType:'json'
			}).done(function(response){
				
				if(response.status == true){
					toastF("Success");
				}else{
					toastF("Save beds: Failed");
				}
				
				$.ajax({
					url: 'http://'+server_ip+'/HARA/database/inc_am.php',
					type: 'GET',
					data: {
						am: included_am,
						id: room_id
					},
					dataType:'json'
				}).done(function(response){
					if(response.status == true){
						toastF("Success");
					}else{
						toastF("save included: Failed");
					}
					stop_load();
				});
				
			});
		});
	});
});
/*------------------------------------------------------------------------*/
function pick(){
	
	window.imagePicker.getPictures(
		function(results) {
			photos = results;
			for (var i = 0; i < results.length; i++) {
				$('#content').append("<div class='col s12 m6' id='"+i+"'> <div class='card'> <div class='card-image'> <img src='"+results[i]+"'> <span class='card-title'></span> <a class='btn-floating halfway-fab waves-effect waves-light red' onclick='view(\""+results[i]+"\")'><i class='material-icons'>search</i></a> </div> <div class='card-content'> <a class='waves-effect waves-light btn bk' onclick='del(\""+i+"\")'>Delete</a> </div> </div> </div>");	
			}
			
		}, function (error) {
			alert('Error: ' + error);
		}
	);
}
/*------------------------------------------------------------------------*/
function view(num){
	PhotoViewer.show(num, 'Room Photo');
}
/*------------------------------------------------------------------------*/
function del(num){
	$('#'+num).remove();
}
/*------------------------------------------------------------------------*/
$('.fixed-action-btn').floatingActionButton({
	toolbarEnabled: true
});
/*------------------------------------------------------------------------*/
function save(){
	/*SpinnerPlugin.activityStart("Saving..", options);

	setInterval(function(){
		SpinnerPlugin.activityStart("Success..", options);
			setInterval(function(){
				SpinnerPlugin.activityStop();
				window.location = "manage-room.html";
			},3000);
	},3000);*/
	//window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess, fail);
	var counter = 0;
	var i = setInterval(function(){
		// do your thing
		window.resolveLocalFileSystemURI(photos[counter], onSuccess, onError);
		counter = counter + 1;
		if(counter == photos.length) {
			clearInterval(i);
		}
	}, 2000);
	
}
/*------------------------------------------------------------------------*/

function onSuccess(fileEntry) {
	load("Uploading...");
    var fd = new FormData();
	fileEntry.file(function(file){
		var reader = new FileReader();
		reader.onloadend = function(e) {
			//salert(JSON.stringify(e));
			
			var imgBlob = new Blob([ this.result ], { type: "image/jpeg" } );
			fd.append('file', imgBlob);
			fd.append('room_id', room_id);
			
			$.ajax({
				type: 'POST',
				url: 'http://'+server_ip+'/HARA/images/upload.php',
				cache:false,
				contentType: false,
				processData: false,
				data: fd
			}).done(function(response){
				stop_load();
			});
		};
		reader.readAsArrayBuffer(file);
	});
	toastF("Success");
}
function onError(err){
	alert(error);
}

$('#upload_form').on('submit', function(e){
	e.preventDefault();
	var formData= new FormData(this);
	
	$.ajax({
		type: 'POST',
		url: 'http://192.168.8.101/upload/upload.php',
		cache:false,
		contentType: false,
		processData: false,
		data: formData
	}).done(function(response){
		alert(response);
	});
	
	alert("submited");
});

