<?php

class db{
	
	public $db;
	public $id;
	
	function __construct(){
		$this->db_connect('localhost','id6392690_renz','salangalich','id6392690_try');
	}
	
	function db_connect($host, $username, $password, $database){
		$this->db = new mysqli($host, $username, $password, $database);
		if($this->db->connect_errno > 0){
			die('Unable to connect to database'. $this->db->connect_error);
		}
	}
	
	function check_changes($table){
		$query = "SELECT * FROM updater WHERE `table`='$table'";
		return $this->get_data($query);
	}
	
	function register_changes($table){
		$this->db->query("UPDATE updater SET counter= counter+1 WHERE `table`='$table'");
	}
	
	function get_data($query){
		$data['items'] = array();
		if($result = $this->db->query($query)){
			while($item = $result->fetch_object()){
				array_push($data['items'], $item);
			}
			if(count($data['items']) > 0){
				$data['result'] = "filled";
			}else{
				$data['result'] = "empty";
			}
			return  $data;
		}
	}
	
	function manipulate_data($query,$table){
		if($this->db->query($query)){
			$this->id = $this->get_last_id();
			$this->register_changes($table);
			return TRUE;
		}
		return $this->db->error;
	}

	function get_last_id(){
		return $this->db->insert_id;
	}
}

