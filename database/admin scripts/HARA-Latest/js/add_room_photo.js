var options = { dimBackground: true };
var hotel_room_info;
var photos[];
/*------------------------------------------------------------------------*/
$(document).ready(function(){
		
	if(isNull(localStorage.getItem('hotel_room'))){
		console.log("pls give room info first");
	}else{
		hotel_room_info = JSON.parse(localStorage.getItem('hotel_room'));
	}

});
/*------------------------------------------------------------------------*/
function pick(){
	window.imagePicker.getPictures(
		function(results) {
			photos = results;
			for (var i = 0; i < results.length; i++) {
				$('#content').append("<div class='col s12 m6' id='"+i+"'> <div class='card'> <div class='card-image'> <img src='"+results[i]+"'> <span class='card-title'></span> <a class='btn-floating halfway-fab waves-effect waves-light red' onclick='view(\""+results[i]+"\")'><i class='material-icons'>search</i></a> </div> <div class='card-content'> <a class='waves-effect waves-light btn bk' onclick='del(\""+i+"\")'>Delete</a> </div> </div> </div>");
			}
		}, function (error) {
			alert('Error: ' + error);
		}
	);
}
/*------------------------------------------------------------------------*/
function view(num){
	PhotoViewer.show(num, 'Room Photo');
}
/*------------------------------------------------------------------------*/
function del(num){
	$('#'+num).remove();
}
/*------------------------------------------------------------------------*/
$('.fixed-action-btn').floatingActionButton({
	toolbarEnabled: true
});
/*------------------------------------------------------------------------*/
function save(){
	/*SpinnerPlugin.activityStart("Saving..", options);

	setInterval(function(){
		SpinnerPlugin.activityStart("Success..", options);
			setInterval(function(){
				SpinnerPlugin.activityStop();
				window.location = "manage-room.html";
			},3000);
	},3000);*/
	console.log(hotel_room_info);
}
/*------------------------------------------------------------------------*/
