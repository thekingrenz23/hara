var manageRoom = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        //get hotel rooms
        $.when(manageRoom.getUserRoom(localStorage.getItem("uid"))).then(function(data){
            if(data.result == "filled"){
                $('#loader').hide();
                for(var i=0; i<data.items.length; i++){
                    $('#cost-li').append("<tr href='#"+data.items[i].id+"-modal' id='"+data.items[i].id+"'> <td>"+data.items[i].name+"</td>  <td>"+data.items[i].units+"</td> </tr>");
                
                    var header = "<div id='"+data.items[i].id+"-modal'> <div class='close-"+data.items[i].id+"-modal center' style='margin-top: 10%'> <i class='material-icons'>cancel</i> </div> <div class='modal-content' style='margin-top: 10%'> <div class='row'> <div class='col s12 m12 l12'>";
                    var photo = "<div class='carousel'>";
                    var body = "<div class='section'><ul class='collection' style='border: none;'><li class='collection-item'>Room name: "+data.items[i].name+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Max Child: "+data.items[i].max_children+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Max adult: "+data.items[i].max_adult+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Room Size: "+data.items[i].size+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Price per Night: "+data.items[i].price_per_night+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li>";
                    
                    for(var x=0; x<data.items[i].photos.items.length; x++){
                        //alert("Connection is slow: Loading: "+response.photos.items[i].location);
                        photo+='<a class="carousel-item" onclick="view(\'' +data.items[i].photos.items[x].location+ '\')"><img src="http://team3gmanager.000webhostapp.com/HARA/images/img/'+data.items[i].photos.items[x].location+'"></a>';
                    }

                    photo+="</div>";

                    for(var y=0; y<data.items[i].amenities.items.length; y++){
                        if(data.items[i].amenities.items[y].price == null){
                            data.items[i].amenities.items[y].price = "FREE";
                        }
                        body+="<li class='collection-item'>"+data.items[i].amenities.items[y].name+"-"+data.items[i].amenities.items[y].price+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li>";
                    }
                    var end_body="</ul> </div> <div class='divider'></div> <div class='section center'> <a class='waves-effect waves-light btn-small bk' onclick='go("+data.items[i].id+")'>DELETE</a> </div> </div> </div> </div> </div>";
                    
                    var all= header+photo+body+end_body;
                    
                    $('#mods').append(all);

                    $('#'+data.items[i].id).animatedModal({
                        animatedIn: 'slideInUp',
                        animatedOut: 'slideOutDown',
                        color: '#ffffff',
                        modalTarget:data.items[i].id+'-modal'
                    }); 
                }

                $(document).ready(function(){
                    $('.carousel').carousel({
                        fullWidth: true,
                    });
                });

            }else{
                $('#loader').hide();
                navigator.notification.alert(
                    'Your hotel doesnt have rooms yet!',
                    function(){},
                    'HARA',
                    'okay'
                );
            }
        }, function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error: '+errorMessage + ' ' + error);
        });
    },
    onBack: function(e){
        e.preventDefault();
    },
    getUserRoom: function(uid){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/get_hotel_room.php',
            type: 'GET',
            data: {
                uid: uid
            },
            dataType: 'json'
        })
    },
    load_modals: function(id, chil, adult, size, refundable, ppn, name){
        $.ajax({
            url: 'http://'+server_ip+'/HARA/database/modal.php',
            type: 'GET',
            data: {
                uid: id
            },
            dataType: 'json'
        }).done(function(response){
            if(refundable == 1){
                refundable = "YES";
            }else{
                refundable = "NO";
            }


            var header = "<div id='"+id+"-modal'> <div class='close-"+id+"-modal center' style='margin-top: 10%'> <i class='material-icons'>cancel</i> </div> <div class='modal-content' style='margin-top: 10%'> <div class='row'> <div class='col s12 m12 l12'>";
            var photo = "<div class='carousel'>";
            var body = "<div class='section'><ul class='collection' style='border: none;'><li class='collection-item'>Room name: "+name+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Max Child: "+chil+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Max adult: "+adult+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Room Size: "+size+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Refundable: "+refundable+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li><li class='collection-item'>Price per Night: "+ppn+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li>";

            for(var i=0; i<response.photos.items.length; i++){
                //alert("Connection is slow: Loading: "+response.photos.items[i].location);
                photo+='<a class="carousel-item" onclick="view(\'' +response.photos.items[i].location+ '\')"><img src="http://team3gmanager.000webhostapp.com/HARA/images/img/'+response.photos.items[i].location+'"></a>';
            }
            
            photo+="</div>";
            
            for(var i=0; i<response.amenity.items.length; i++){
                if(response.amenity.items[i].price == null){
                    response.amenity.items[i].price = "FREE";
                }
                body+="<li class='collection-item'>"+response.amenity.items[i].name+"-"+response.amenity.items[i].price+" <i class='secondary-content material-icons circle'>format_list_bulleted</i></li>";
            }
            var end_body="</ul> </div> <div class='divider'></div> <div class='section center'> <a class='waves-effect waves-light btn-small bk' onclick='go()'>Update</a> </div> </div> </div> </div> </div>";
            
            var all= header+photo+body+end_body;
            
            $('#mods').append(all);
            
            $('#'+id).animatedModal({
                animatedIn: 'slideInUp',
                animatedOut: 'slideOutDown',
                color: '#ffffff',
                modalTarget:id+'-modal'
            });
            
            $(document).ready(function(){
                $('.carousel').carousel({
                    fullWidth: true,
                });
            });
        });
    }
}
manageRoom.init();

function view(num){
    PhotoViewer.show('http://team3gmanager.000webhostapp.com/HARA/images/img/'+num, 'Room Photo');
}


function go(id){
    navigator.notification.confirm(
        'Do you really want to delete this room?', // message
         function(index){
            if(index == 1){
                $.ajax({
                    url: 'http://'+server_ip+'/HARA/database/delete.php',
                    type: 'GET',
                    data: {
                        id: id
                    },
                    dataType: 'json',
                    error: function(xhr, status, error){
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        window.location = "manage-room.html";
                    }
                }).done(function(data){
                
                });
            }
         },            // callback to invoke with index of button pressed
        'HARA',           // title
        ['yes','cancel']     // buttonLabels
    );
}