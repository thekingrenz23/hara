$('#next').click(function(){
	
	var	name= $('#room_name').val();
	var	price= $('#room_price').val();
	var	cents= $('#room_cents').val();
	var	child= $('#room_child').val();
	var	adult= $('#room_adult').val();
	var	length= $('#room_length').val();
	var	width= $('#room_width').val();
	var	amenity= M.Chips.getInstance($('#amenity'));
	var	bed= M.Chips.getInstance($('#bed'));

	if(isFilled([name, price, cents, child, adult, length, width])){
		if(isFilled_tag(amenity.chipsData) && isFilled_tag(bed.chipsData)){
			var room_info = '{"name":"'+name+'","price": "'+price+'", "cents": "'+cents+'", "child": "'+child+'", "adult":"'+adult+'", "length": "'+length+'", "width": "'+width+'", "amenity":"'+amenity+'", "bed": "'+bed+'"}';
			
			var json_room = JSON.parse(room_info);			

			localStorage.setItem('hotel_room',JSON.stringify(json_room));
			window.location = "add-room-photo.html";
		}else{
			console.log("pls fill all fields");
		}	
	}else{
		console.log("pls fill all fields");
	}
	
});
