$(document).on('deviceready', function(){
	StatusBar.backgroundColorByHexString("#ff0000");
});

$(document).on('backbutton', function(e){
	e.preventDefault();
});

var lat = null;
var lng = null;

$("#submit").click(function(){
	load("Loading..");
	
	var hotel_name = $('#hotel_name').val();
	var owner_firstname = $('#owner_firstname').val();
	var owner_lastname = $('#owner_lastname').val();
	var owner_bdate = $('#owner_bdate').val();
	var owner_address = $('#owner_address').val();
	var hotel_mnumber = $('#hotel_mnumber').val();
	var hotel_tnumber = $('#hotel_tnumber').val();
	var hotel_email = $('#hotel_email').val();
	var password = $('#password').val();
	var cpassword = $('#cpassword').val();
	var domain = $('#domain').val();
	var isForward = $('#isForward').prop('checked');
	var forward;
	if(isForward){
		forward = 1;
	}else{
		forward = 0;
	}
	
	if(isNull(lat) && isNull(lng)){		
		if(isFilled([owner_address])){
			$.ajax({
				url: 'https://maps.googleapis.com/maps/api/geocode/json',
				type: 'GET',
				dataType: 'json',
				data: {
					address: owner_address.replace(/\s/g, ''),
					key: 'AIzaSyCeaDB-C0Z0sxoOjSlip57IhVNfJgOie1o'
				}
			}).done(function(response){
				lat = response.results[0].geometry.location.lat;
				lng = response.results[0].geometry.location.lng;
				
				if(response.status = "OK"){
					
					$.ajax({
						url: 'http://'+server_ip+'/HARA/database/check_dup_email.php',
						type: 'POST',
						dataType: 'json',
						data: {
							email: hotel_email
						}
					}).done(function(response){
						
						if(response.exist == true){
							stop_load();
							toastF("Email Already Exist");
						}else{
							if(isEquals(password,cpassword)){
								if(isFilled([hotel_name, owner_firstname, owner_lastname, owner_bdate, owner_address, hotel_mnumber, hotel_tnumber, hotel_email, password, cpassword, domain])){
									$.ajax({
										type: 'POST',
										url: 'http://'+server_ip+'/HARA/database/register_hotelier.php',
										data:{
											hotel_name : hotel_name,
											owner_firstname : owner_firstname,
											owner_lastname : owner_lastname,
											owner_bdate : owner_bdate,
											owner_address: owner_address,
											hotel_mnumber : hotel_mnumber,
											hotel_tnumber: hotel_tnumber,
											hotel_email: hotel_email,
											password: password,
											domain: domain,
											isForward : forward,
											lat: lat,
											lng: lng
										},
										dataType: 'json'
									}).done(function(response){
										
										if(response.account == true && response.profile == true){
											stop_load();
											toastF("Success");
											window.location="index.html";
										}else{
											stop_load();
											toastF("Failed");
										}
									});
								}else{
									stop_load();
									toastF("Pls. Fill all Fields");
								}
							}else{
								stop_load();
								toastF("Password not match");
							}
						}
						
					});
				}else{
					stop_load();
					toastF("Status Failed");
				}
			});
		}else{
			stop_load();
			toastF("Pls. Fill Address");
		}
	}else{
		
		
		$.ajax({
			url: 'http://'+server_ip+'/HARA/database/check_dup_email.php',
			type: 'POST',
			dataType: 'json',
			data: {
				email: hotel_email
			}
		}).done(function(response){
			
			if(response.exist == true){
				stop_load();
				toastF("Email Already Exist");
			}else{
				if(isEquals(password,cpassword)){
					if(isFilled([hotel_name, owner_firstname, owner_lastname, owner_bdate, owner_address, hotel_mnumber, hotel_tnumber, hotel_email, password, cpassword, domain])){
						$.ajax({
							type: 'POST',
							url: 'http://'+server_ip+'/HARA/database/register_hotelier.php',
							data:{
								hotel_name : hotel_name,
								owner_firstname : owner_firstname,
								owner_lastname : owner_lastname,
								owner_bdate : owner_bdate,
								owner_address: owner_address,
								hotel_mnumber : hotel_mnumber,
								hotel_tnumber: hotel_tnumber,
								hotel_email: hotel_email,
								password: password,
								domain: domain,
								isForward : forward,
								lat: lat,
								lng: lng
							},
							dataType: 'json'
						}).done(function(response){
							
							if(response.account == true && response.profile == true){
								stop_load();
								toastF("Success");
								window.location="index.html";
							}else{
								stop_load();
								toastF("Failed");
							}
						});
					}else{
						stop_load();
						toastF("Pls. Fill all Fields");
					}
				}else{
					stop_load();
					toastF("Password not match");
				}
			}
			
		});
	}
});


$('#find').click(function(){
	
	cordova.plugins.diagnostic.isLocationEnabled(function(enabled){
		if(!enabled){
			window.cordova.plugins.settings.open("location", function() {
				
			},
			function () {
				toastF("Error opening location settings");
			});
		}else{
			load("Finding..");
			navigator.geolocation.getCurrentPosition(function(position){
				lat = position.coords.latitude;
				lng = position.coords.longitude;

					$.ajax({
						url: 'https://maps.googleapis.com/maps/api/geocode/json',
						type: 'GET',
						data:{
							latlng: lat+','+lng,
							key: "AIzaSyCeaDB-C0Z0sxoOjSlip57IhVNfJgOie1o"
						},
						dataType: 'json'
					}).done(function(response){
						stop_load();
						$('#owner_address').val(response.results[0].formatted_address);
						toastF("Success");
					});
			}, function(){
				stop_load();
				toastF("Error Getting you location");
			});
		}
	}, function(error){
		toastF("Error checking location status");
	});
	
});


