var registerTraveler = {
    init: function(){
        $(document).on('deviceready', this.onDeviceReady.bind(this));
        $(document).on('backbutton', this.onBack.bind(this));
    },
    onDeviceReady: function(){
        //StatusBar.backgroundColorByHexString("#ff0000");

        $('#submit').click(function(){
           registerTraveler.saveTraveler(); 
        });


    },
    onBack: function(e){
        e.preventDefault();
    },
    saveTraveler: function(){
        load("Registering..");
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var bdate = $('#bdate').val();
        var address = $('#address').val();
        var mnumber = $('#mnumber').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var cpassword = $('#cpassword').val();

        $.when(registerTraveler.checkDup(email)).then(function(data){
            if(data.exist == true){
                stop_load();
                toastF("Email Already used");
            }else{
                if(isFilled([fname, lname, bdate, address, mnumber, email, password, cpassword])){
                    if(isEquals(password,cpassword)){
                        $.when(registerTraveler.registerTraveler()).then(function(data){
                            stop_load();
                            if(data.account == true && data.profile == true){
                                toastF("Success");
                                localStorage.setItem("uid", data.id);
                                window.location = "home.html";
                            }else{
                                toastF("Failed");
                            }
                        }, function(xhr, status, error){
                            stop_load();
                            var errorMessage = xhr.status + ': ' + xhr.statusText;
                            toastF('Error: '+errorMessage + ' ' + error);
                        });
                    }else{
                        toastF("Password not matched");
                    }
                }else{
                    toastF("Pls. Fill all fields");
                }
            }
        }, function(xhr, status, error){
            stop_load();
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            toastF('Error: '+errorMessage + ' ' + error);
        });
    },
    checkDup: function(email){
        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/check_dup_email.php',
            type: 'GET',
            data: {
                email : email
            },
            dataType: 'json'
        })
    },
    registerTraveler: function(){
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var bdate = $('#bdate').val();
        var address = $('#address').val();
        var mnumber = $('#mnumber').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var cpassword = $('#cpassword').val();

        return $.ajax({
            url: 'http://'+server_ip+'/HARA/database/register_traveler.php',
            type: 'GET',
            data: {
                fname: fname,
                lname: lname,
                bdate: bdate,
                address: address,
                mnumber: mnumber,
                email: email,
                password: password
            },
            dataType: 'json'
        })
    }
}
registerTraveler.init();