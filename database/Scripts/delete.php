<?php
    header('Access-Control-Allow-Origin:*');
    require_once('../../database.php');

    $wire = new db();

    //Get passed data here
    $id = $_GET['id'];

    $query = "DELETE FROM room WHERE id = $id";

    //Returns the data['result'] = 'filled' | 'empty', data['items'] = [array of items]
    //$data['rq']=$wire->get_data($query);

    //Returns the true is success | false if failed
    $data['del']=$wire->manipulate_data($query, 'room');

    echo json_encode($data);