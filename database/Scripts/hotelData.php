<?php
    header('Access-Control-Allow-Origin:*');
    require_once('../../database.php');

    $wire = new db();

    //Get passed data here
    $uid = $_GET['uid'];
    
    $query = "select *, cover_photo.location as img,ROUND((average_rating.cleanliness + average_rating.service + average_rating.location + average_rating.rooms)/4,1) as average from cover_photo ,hotel, average_rating, location, account where hotel.id = average_rating.hotel_id and cover_photo.hotel_id = hotel.id and account.id=hotel.id and location.hotel_id = hotel.id and hotel.id = $uid";

    //Returns the data['result'] = 'filled' | 'empty', data['items'] = [array of items]
    $data['hotel']=$wire->get_data($query);

    $query = "SELECT DISTINCT amenity_type.name, amenity_type.icon FROM amenity, room, amenity_type WHERE room.hotel_id=$uid AND amenity.amenity_id = amenity_type.id";

    $data['ams'] = $wire->get_data($query);
    //Returns the true is success | false if failed
    //$data['rq']=$wire->manipulate_data($query, 'table_name');

    echo json_encode($data);